package fr.iiztp.languages;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.iiztp.utils.Functions;


public class Languages
{
	//General
	private String toAvoidSpam = "To avoid spam, you must send commands in the channel {bot-commands} or with me in private !";
	private String showBalance = "You have `{money}Ⴥ` on your account !";
	private String showLevel = "You are level **{level}**";
	private String mute = "You **{mute}** the bot !";
	private String attention = "Pay attention while trading, it is at your peril";
	
	//Words
	private String lower = "lower";
	private String greater = "greater";
	private String amount = "amount";
	private String buyAndSell = "Buy/Sell";
	private String price = "price";
	
	//General exception
	private String chartArgument = "You need to put the chart you want as an argument";
	private String unknownChart = "Unknown Chart !";
	private String invalidPrice = "You need to put the price 'format : `1.72`'";
	private String mentionUser = "You must mention a user !";
	private String invalidUser = "Invalid User !";
	private String invalidAmount = "Invalid amount !";
	private String notEnoughMoney = "You do not have enough money !";
	private String noIndex = "You must put the index of the trade/alert as an argument !";
	
	
	//Alerts&Trades
	private String noTrigger = "You need to put the trigger of the alert, available on {links}";
	private String openAlert = "**You opened an alert on {symbol}**";
	private String noActiveAlerts = "You do not have any active alerts !";
	private String noActiveTrades = "You do not have any active trades !";
	private String greaterThanFive = "The minimum to trade is 5$ !";
	private String tradeException = "You need to put the {exception} as an argument !";
	private String changeLever = "Due to high volatility on this chart the lever has been reduced to **{lever}**";
	private String leverMustBe = "The lever/multiplier must be betweer 1 and 200";
	private Map<String, String> embedTrade = new HashMap<>();
	
	//Give
	private String giveAmountException = "Amount must be lower than 50 and greater than 0 !";
	private String giveMessage = "{author} gave **{money}**Ⴥ to {mentionnedUser}";
	
	//Close
	private String titleAlert = "Alert closed !";
	private String descriptionAlert = "You closed your alert on {symbol}";
	private String titleTrade = "Trade closed !";
	private String descriptionTrade = "You closed your trade on {symbol}";
	private String profitTrade = "Profit : ";
	private String lossTrade = "Loss : ";
	
	//CommandTimer
	private String commandTimerException = "You already used this command, next time you will be able to do it again in **{d}** days, **{h}**h**{m}**m**{s}**s !";
	
	//Help
	private Map<String, String> help = new HashMap<>();
	
	//Global
	private Map<String, String> global = new HashMap<>();
	
	//Price
	private String priceGet = "The current price of `{symbol}` is `{price}`";
	
	//Jackpot
	private Map<String, String> jackpot = new HashMap<>();
	
	//Stats
	private Map<String, String> stats = new HashMap<>();
	
	//add
	private String addNoAmount = "You must put how much you want to add as the second argument !";
	private String confirmAdd = "You've just added **{amount}**Ⴥ to your active trade !";
	
	//SL/TP
	private String slMustBe = "The price of the {tpsl} must be {lw} than the actual price";
	private String slTpRedefined = "{tpsl} (re)defined at the price of **{price}**";
	private String slpTppRedefined = "{tpsl} (re)defined at the p/l of **{price}**Ⴥ";
	
	public Languages()
	{
		//Global Set
		global.put("title", "Global Stats");
		global.put("description", "Here are some global stats of this trade server");
		global.put("field1", "Money in circulation : ");
		global.put("field2", "Money owned by the Sociole Central Bank : ");
		global.put("field3", "Money owned by the people of Socialab : ");
		global.put("field4", "Population of Socialab (using Sociole) : ");
		global.put("field5", "The ownings of the best trader in % : ");
		
		//Help Set
		help.put("title", "Here's the help : ");
		help.put("description", "Note that it may be some commands that aren't available because of your actual rank/lvl");
		help.put("price", "Returns the actual price of the symbol, symbols are in the channel {links}");
		help.put("online", "Checks if the bot that gives prices in real time is up");
		help.put("give", "Give from 1 to 50 Socioles to the mentionned player (once per week)");
		help.put("global", "Returns the global stats/numbers of the server");
		help.put("jackpot", "See the available jackpot");
		help.put("money", "Returns what you have on the account to trade");
		help.put("mute", "Allows you to stop receiving the daily message");
		help.put("trade", "Makes a trade with the symbol you want to the up or down with the amount you want and the lever you want between 1 and 200*");
		help.put("active", "Shows all your active trades with their indexes");
		help.put("close", "Close the trade at the index position");
		help.put("sl", "Places a Stop Loss on your trade at the price you want");
		help.put("tp", "Places a Take Profit on your trade at the price you want");
		help.put("slp", "Places a Stop Loss on your trade at the amount won/lost you want");
		help.put("tpp", "Places a Take Profit on your trade at the amount won/lost you want");
		help.put("stats", "Shows your stats, and it's not good, really !");
		help.put("add", "Adds an amount in an existing trade to avoid the near-close or not of this one, change the enter price to a new balanced one");
		help.put("footer", "*Note that the lever is a multiplier, you will gain faster but you will lose stronger");
		
		//Jackpot Set
		jackpot.put("description", "Amount to win : ***{amount}***");
		jackpot.put("field1", "Required : ");
		jackpot.put("field2", "Available : ");
		jackpot.put("field3", "every **24** hours");
		jackpot.put("footer", "**Put anything just after the command to confirm to play !**");
		jackpot.put("win", "What the luck ? {user} just won a jackpot of **{amount}**Ⴥ !");
		jackpot.put("lose", "Unfortunately you lost ! Better luck next time !");
		
		//Stats Set
		stats.put("title", "Trader stats : ");
		stats.put("description", "Here are some stats of you");
		stats.put("field1", "Number of total trades");
		stats.put("field2", "Number of trades you succeeded");
		stats.put("field3", "Number of trades you failed");
		stats.put("field4", "Ratio (in %)");
		stats.put("field5", "Number of active trades/alerts");
		stats.put("field6", "Best amount won by trading");
		stats.put("field7", "Worst amount lost by trading");
		stats.put("field8", "Your ownings on the server (in %)");
		
		//EmbedTrade Set
		embedTrade.put("titleCreate", "Trade Defined !");
		embedTrade.put("bull", "Bull Market to see !");
		embedTrade.put("bear", "Bear Market to see !");
		embedTrade.put("decriptionCreate", "Congrats, you opened a trade on {symbol}");
		embedTrade.put("symbol", "Symbol : ");
		embedTrade.put("decriptionCreate", "Congrats, you opened a trade on {symbol}");
		embedTrade.put("value", "Value (at time of buy) : ");
		embedTrade.put("valuenow", "Value now : ");
		embedTrade.put("quantity", "Quantity : ");
		embedTrade.put("buysell", "Buy/Sell : ");
		embedTrade.put("lever", "Multiplier/Lever : ");
		embedTrade.put("commission", "Commission taken :");
		embedTrade.put("tradealertCreate", "TradeAlert Defined !");
		embedTrade.put("tradealertDescription", "Congrats, you opened a TradeAlert on {symbol}. This trade will be opened when the alert is triggered !");
		embedTrade.put("tradealertTrigger", "Triggered when : ");
		embedTrade.put("description", "Your trade on **{symbol}** : **{sign}{profit}**Ⴥ");
		
		Functions.createJson(this, "languages/", "default");
	}
	public String getPriceGet() {
		return priceGet;
	}
	public void setPriceGet(String priceGet) {
		this.priceGet = priceGet;
	}
	public void setLeverMustBe(String leverMustBe) {
		this.leverMustBe = leverMustBe;
	}
	public String getLeverMustBe() {
		return leverMustBe;
	}
	public void setEmbedTrade(Map<String, String> embedTrade) {
		this.embedTrade = embedTrade;
	}
	public void setLower(String lower) {
		this.lower = lower;
	}
	public Map<String, String> getEmbedTrade() {
		return embedTrade;
	}
	public void setUnknownChart(String unknownChart) {
		this.unknownChart = unknownChart;
	}
	public String getUnknownChart() {
		return unknownChart;
	}
	public void setToAvoidSpam(String toAvoidSpam) {
		this.toAvoidSpam = toAvoidSpam;
	}
	public String getToAvoidSpam() {
		return toAvoidSpam;
	}
	public void setShowBalance(String showBalance) {
		this.showBalance = showBalance;
	}
	public String getShowBalance() {
		return showBalance;
	}
	public void setShowLevel(String showLevel) {
		this.showLevel = showLevel;
	}
	public String getShowLevel() {
		return showLevel;
	}
	public void setMute(String mute) {
		this.mute = mute;
	}
	public String getMute() {
		return mute;
	}
	public Map<String, String> getHelp() {
		return help;
	}
	public void setHelp(Map<String, String> help) {
		this.help = help;
	}
	public void setChartArgument(String chartArgument) {
		this.chartArgument = chartArgument;
	}
	public String getChartArgument() {
		return chartArgument;
	}
	public void setNoTrigger(String noTrigger) {
		this.noTrigger = noTrigger;
	}
	public String getNoTrigger() {
		return noTrigger;
	}
	public void setInvalidPrice(String invalidPrice) {
		this.invalidPrice = invalidPrice;
	}
	public String getInvalidPrice() {
		return invalidPrice;
	}
	public void setOpenAlert(String openAlert) {
		this.openAlert = openAlert;
	}
	public String getOpenAlert() {
		return openAlert;
	}
	public void setNoActiveAlerts(String noActiveAlerts) {
		this.noActiveAlerts = noActiveAlerts;
	}
	public String getNoActiveAlerts() {
		return noActiveAlerts;
	}
	public void setMentionUser(String mentionUser) {
		this.mentionUser = mentionUser;
	}
	public String getMentionUser() {
		return mentionUser;
	}
	public void setInvalidUser(String invalidUser) {
		this.invalidUser = invalidUser;
	}
	public String getInvalidUser() {
		return invalidUser;
	}
	public void setGiveAmountException(String giveAmountException) {
		this.giveAmountException = giveAmountException;
	}
	public String getGiveAmountException() {
		return giveAmountException;
	}
	public String getCommandTimerException() {
		return commandTimerException;
	}
	public void setCommandTimerException(String commandTimerException) {
		this.commandTimerException = commandTimerException;
	}
	public void setGiveMessage(String giveMessage) {
		this.giveMessage = giveMessage;
	}
	public String getGiveMessage() {
		return giveMessage;
	}
	public void setNotEnoughMoney(String notEnoughMoney) {
		this.notEnoughMoney = notEnoughMoney;
	}
	public String getNotEnoughMoney() {
		return notEnoughMoney;
	}
	public void setGlobal(Map<String, String> global) {
		this.global = global;
	}
	public Map<String, String> getGlobal() {
		return global;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getPrice() {
		return price;
	}
	public void setJackpot(Map<String, String> jackpot) {
		this.jackpot = jackpot;
	}
	public Map<String, String> getJackpot() {
		return jackpot;
	}
	public void setStats(Map<String, String> stats) {
		this.stats = stats;
	}
	public Map<String, String> getStats() {
		return stats;
	}
	public void setNoActiveTrades(String noActiveTrades) {
		this.noActiveTrades = noActiveTrades;
	}
	public String getNoActiveTrades() {
		return noActiveTrades;
	}
	public void setAddNoAmount(String addNoAmount) {
		this.addNoAmount = addNoAmount;
	}
	public String getAddNoAmount() {
		return addNoAmount;
	}
	public void setNoIndex(String noIndex) {
		this.noIndex = noIndex;
	}
	public String getNoIndex() {
		return noIndex;
	}
	public void setConfirmAdd(String confirmAdd) {
		this.confirmAdd = confirmAdd;
	}
	public String getConfirmAdd() {
		return confirmAdd;
	}
	public void setAttention(String attention) {
		this.attention = attention;
	}
	public String getAttention() {
		return attention;
	}
	public void setDescriptionAlert(String descriptionAlert) {
		this.descriptionAlert = descriptionAlert;
	}
	public void setDescriptionTrade(String descriptionTrade) {
		this.descriptionTrade = descriptionTrade;
	}
	public void setLossTrade(String lossTrade) {
		this.lossTrade = lossTrade;
	}
	public void setProfitTrade(String profitTrade) {
		this.profitTrade = profitTrade;
	}
	public void setTitleAlert(String titleAlert) {
		this.titleAlert = titleAlert;
	}
	public void setTitleTrade(String titleTrade) {
		this.titleTrade = titleTrade;
	}
	public String getDescriptionAlert() {
		return descriptionAlert;
	}
	public String getDescriptionTrade() {
		return descriptionTrade;
	}
	public String getLossTrade() {
		return lossTrade;
	}
	public String getProfitTrade() {
		return profitTrade;
	}
	public String getTitleAlert() {
		return titleAlert;
	}
	public String getTitleTrade() {
		return titleTrade;
	}
	public String getSlMustBe() {
		return slMustBe;
	}
	public void setSlMustBe(String slMustBe) {
		this.slMustBe = slMustBe;
	}
	public String getSlpTppRedefined() {
		return slpTppRedefined;
	}
	public String getSlTpRedefined() {
		return slTpRedefined;
	}
	public void setSlpTppRedefined(String slpTppRedefined) {
		this.slpTppRedefined = slpTppRedefined;
	}
	public void setSlTpRedefined(String slTpRedefined) {
		this.slTpRedefined = slTpRedefined;
	}
	public void setTradeException(String tradeException) {
		this.tradeException = tradeException;
	}
	public String getTradeException() {
		return tradeException;
	}
	public void setInvalidAmount(String invalidAmount) {
		this.invalidAmount = invalidAmount;
	}
	public String getInvalidAmount() {
		return invalidAmount;
	}
	public void setGreaterThanFive(String greaterThanFive) {
		this.greaterThanFive = greaterThanFive;
	}
	public String getGreaterThanFive() {
		return greaterThanFive;
	}
	public String getAmount() {
		return amount;
	}
	public String getBuyAndSell() {
		return buyAndSell;
	}
	public String getGreater() {
		return greater;
	}
	public String getLower() {
		return lower;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public void setBuyAndSell(String buyAndSell) {
		this.buyAndSell = buyAndSell;
	}
	public void setGreater(String greater) {
		this.greater = greater;
	}
	public String getChangeLever() {
		return changeLever;
	}
	public void setChangeLever(String changeLever) {
		this.changeLever = changeLever;
	}
	public Languages setLanguage()
	{
		String json = "";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try(BufferedReader reader = new BufferedReader(new FileReader("languages/" + this.getClass().getSimpleName() + ".json")))
		{
			String line = "";
			while ((line = reader.readLine()) != null)
			{
				json  += line;
			}
		}
		catch(Exception e)
		{
			System.out.println("Language Exception error !");
			return null;
		}
		
		return gson.fromJson(json, this.getClass());
	}
}
