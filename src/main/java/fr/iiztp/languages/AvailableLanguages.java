package fr.iiztp.languages;

import fr.iiztp.languages.langs.*;

public enum AvailableLanguages
{
	EN_US(new English(), "691343908315332639"),
	FR_FR(new French(), "699268155893219389"),
	DE_DE(new German(), "705007582594465802");
	
	private Languages language;
	private String textChannelId;
	
	private AvailableLanguages(Languages lang, String id)
	{
		this.language = lang.setLanguage();
		this.textChannelId = id;
	}
	public void setLanguage(Languages language) {
		this.language = language;
	}
	public Languages getLanguage()
	{
		return language;
	}
	public void setTextChannelId(String textChannelId) {
		this.textChannelId = textChannelId;
	}
	public String getTextChannelId() {
		return textChannelId;
	}
	public static Languages getLanguage(String id)
	{
		Languages lang = null;
		if(id.equals(EN_US.getTextChannelId()))
		{
			lang = AvailableLanguages.EN_US.getLanguage();
		}
		if(id.equals(FR_FR.getTextChannelId()))
		{
			lang = AvailableLanguages.FR_FR.getLanguage();
		}
		if(id.equals(DE_DE.getTextChannelId()))
		{
			lang = AvailableLanguages.DE_DE.getLanguage();
		}
		return lang;
	}
	public static AvailableLanguages getAvailableLanguages(Languages lang)
	{
		for(int i = 0; i < AvailableLanguages.values().length; i++)
		{
			if(AvailableLanguages.values()[i].getLanguage().equals(lang))
			{
				return AvailableLanguages.values()[i];
			}
		}
		return null;
	}
}
