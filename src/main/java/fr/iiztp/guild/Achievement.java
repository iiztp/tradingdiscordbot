package fr.iiztp.guild;

import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

public enum Achievement {

	REACH_HUNDRED_SOCIOLE("Become a hundredaire", 1,true, false),
	REACH_THOUSAND_SOCIOLE("Become a thousandaire", 10,true, false),
	REACH_HUNDRED_THOUSAND_SOCIOLE("Become a hundred thousandaire", 100, true, false),
	REACH_MILLION_SOCIOLE("Become a millionaire", 10000, true, true),
	REACH_BILLION_SOCIOLE("Become a billionaire", 100000, true, true),
    FIRST_TRADE("Do the first trade", 10, true, false),
    FIVE_TRADE("", 0, true, false),
    TEN_TRADE("", 0, true, false),
    TWENTY_TRADE("", 0, true, false),
    THIRTY_TRADE("", 0, true, false),
    FOURTY_TRADE("", 0, true, false),
    FIFTY_TRADE("", 0, true, false),
    SIXTY_TRADE("", 0, true, false),
    SEVENTY_TRADE("", 0, true, false),
    EIGHTY_TRADE("", 0, true, false),
    NINETY_TRADE("", 0, true, false),
    HUNDRED_TRADE("A hundred trades in the pocket", 100, true, false),
    THOUSAND_TRADE("A thousand trades in the jacket", 1000, true, false),
    FIRST_FAILED_TRADE("Everything's begin with a failure", 10, true, false),
    TEN_FAILED_TRADE("Good, you see now that it is not that easy !", 100, true, false),
    HUNDRED_FAILED_TRADE("My god, how is this even possible ?", 1000, true, false),
	FIRST_SUCCEEDED_TRADE("Just some luck, it will vanish", 10, true, false),
    TEN_SUCCEEDED_TRADE("Good, you see now that it is not that easy !", 100, true, false),
    HUNDRED_SUCCEEDED_TRADE("My god, how is this even possible ?", 1000, true, false),
	REACH_LEVEL_FIVE("High reached Five", 50, false, true),
	REACH_LEVEL_TEN("Ten out of ten", 100, false, true),
	REACH_LEVEL_TWENTY("Reach level 20", 200, false, true),
	REACH_LEVEL_THIRTY("Reach level 30", 300, false, true),
	REACH_LEVEL_FOURTY("Reach level 40", 400, false, true),
	REACH_LEVEL_FIFTY("It is not about cents, fifty cents", 500, false, true),
	MUST_CLOSE_TRADE("That is not your day !", 20, true, false),
	DEFINE_STOPLOSS("Take less risk, put a stoploss (SL)", 50, true, false),
	CLOSED_STOPLOSS("Oof, that was close", 50, true, false),
	DEFINE_TAKEPROFIT("Take less risk, put a takeprofit (TP)", 50, true, false),
	CLOSED_TAKEPROFIT("Nothing is better than being secured", 50, true, false),
	RATIO_GREATER_THAN_ZERO_TEN("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_TWENTY("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_THIRTY("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_FOURTY("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_FIFTY("One trade out of two", 500, true, false),
	RATIO_GREATER_THAN_ZERO_SIXTY("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_SEVENTY("", 0, true, false),
	RATIO_GREATER_THAN_ZERO_SEVENTY_FIVE("Three trade out of four", 7500, true, false),
	SHOW_ACTIVE_TRADES("Show me da trade", 10, true, false),
	HAVE_TEN_ACTIVE_TRADES("The trades power !", 50, true, false),
	HUNDRED_SOCIOLE_TRADE("Wow, you are rich enough for that ?", 50, true, false),
	THOUSAND_SOCIOLE_TRADE("Spreading out my richness", 500, true, false),
	HUNDRED_THOUSAND_SOCIOLE_TRADE("Wow, you are rich enough for that ?", 50000, true, false),
	MILLION_SOCIOLE_TRADE("You found da wey aren't ya ?", 500000,true, false),
	BILLION_SOCIOLE_TRADE("How the fack is this even possible ?", 500000000, true, false),
	LOSE_HUNDRED_SOCIOLES("Euphoria", 10, true, false),
	LOSE_THOUSAND_SOCIOLES("Sadness", 100, true, false),
	LOSE_HUNDRED_THOUSAND_SOCIOLES("Send halp, lost more than my mind", 1000, true, false),
	LOSE_MILLION_SOCIOLES("FFFUUUUUUU", 10000, true, false),
	LOSE_BILLION_SOCIOLES("UNCONSCIOUS", 100000, true, false),
	WIN_HUNDRED_SOCIOLES("GOT LUCKY", 10, true, false),
	WIN_THOUSAND_SOCIOLES("NEVER ENOUGH", 100, true, false),
	WIN_HUNDRED_THOUSAND_SOCIOLES("Where is my pocket ?", 1000, true, false),
	WIN_MILLION_SOCIOLES("Swimming pool full of Socioles", 10000, true, false),
	WIN_BILLION_SOCIOLES("Buy a planet", 100000, true, false),
	ALERT_ME("Don't want to lose this opportunity", 10, true, false);

    private String name;
    private int reward;
    private boolean levelUp;
    private boolean roleUp;

    private Achievement(String name, int reward, boolean levelUp, boolean roleUp) {
        this.name = name;
        this.reward = reward;
        this.levelUp = levelUp;
        this.roleUp = roleUp;
    }
    
    public String getName()
    {
    	return this.name;
    }
    
    public int getReward()
    {
    	return this.reward;
    }
    
    public boolean getLevelUp()
    {
    	return this.levelUp;
    }
    
    public boolean getRoleUp()
    {
    	return this.roleUp;
    }
    public void addAchievement(Trader user)
    {
    	user.setBalance(user.getBalance() + this.getReward());
    	Main.botTrader.setBalance(Main.botTrader.getBalance() - this.getReward());
    	Main.botTrader.saveUser();
    	if(!this.getName().isEmpty())
    	{
    		Main.bot.getTextChannelById("692483562804346962").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + " just achieved '**" + this.getName() + "**' ! He wins **" + this.getReward() + "**Ⴥ for that.").complete();
    	}
    	if(this.getLevelUp())
    	{
    		user.levelUp();
    		Main.bot.getTextChannelById("692483562804346962").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + " just **leveled up** ! He is now **level " + user.getLevel() + "** !").complete();
    	}
    	if(this.getRoleUp())
    	{
    		Member member = Main.bot.getGuildById("312631087480438794").getMemberById(user.getId());
    		Role roleToRemove = Main.bot.getRoleById(user.getRank().getId());
    		Role roleToAdd = Main.bot.getRoleById(Rank.getRankByIndex(user.getRank().getIndex() + 1).getId());
    	
    		Main.bot.getGuildById("312631087480438794").getController().removeSingleRoleFromMember( member, roleToRemove).complete();
    		Main.bot.getGuildById("312631087480438794").getController().addSingleRoleToMember( member, roleToAdd).complete();
    		
    		user.setRank(Rank.getRankByIndex(user.getRank().getIndex() + 1));
    		
    		Main.bot.getTextChannelById("692483562804346962").sendMessage("Congrats ! " + Main.bot.getUserById(user.getId()).getAsMention() + " just **ranked up** ! He is now **" + Main.bot.getRoleById(user.getRank().getId()).getName() + "** !").complete();
    	}
    }
    public void deleteAchievement(Trader user)
    {
    	Member member = Main.bot.getGuildById("312631087480438794").getMemberById(user.getId());
    	user.setBalance(user.getBalance() - this.getReward());
    	Main.botTrader.setBalance(Main.botTrader.getBalance() + this.getReward());
    	Main.botTrader.saveUser();
    	if(this.getRoleUp())
    	{
    		Role roleToRemove = Main.bot.getRoleById(user.getRank().getId());
    		Role roleToAdd = Main.bot.getRoleById(Rank.getRankByIndex(user.getRank().getIndex() - 1).getId());
    	
    		Main.bot.getGuildById("312631087480438794").getController().removeSingleRoleFromMember( member, roleToRemove).complete();
    		Main.bot.getGuildById("312631087480438794").getController().addSingleRoleToMember( member, roleToAdd).complete();
    		user.setRank(Rank.getRankByIndex(user.getRank().getIndex() - 1));
    		
    		Main.bot.getTextChannelById("691343908315332639").sendMessage(member.getAsMention() + ", you've been RoleDowned by an admin !").complete();
    	}
    	if(this.getLevelUp())
    	{
    		user.setLevel(user.getLevel() - 1);
    		Main.bot.getTextChannelById("691343908315332639").sendMessage(member.getAsMention() + ", you've been level downed by an admin !").complete();
    	}
    	Main.bot.getTextChannelById("691343908315332639").sendMessage(member.getAsMention() + ", an achievement has been taken away from you by an admin, -" + this.getReward() + "Ⴥ has been taken from your balance !").complete();
    }
}