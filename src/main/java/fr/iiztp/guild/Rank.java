package fr.iiztp.guild;

public enum Rank
{
	DEFAULT("690699942502727771", 0),
	POOR("690861155521921025", 1),
	STUDENT("690860766248566834", 2),
	TRAINEE("690860357698322432", 3),
	PART_TIME_EMPLOYEE("690860996411260938", 4),
	FULL_TIME_EMPLOYEE("690861049473400852", 5),
	RICH("690861425597349889", 6),
	MILLIONAIRE("690861487669116998", 7),
	BILLIONAIRE("690861618690523178", 8),
	PROFESSIONAL("312635562207084550", 9),
	LEADER_OF_THE_WORLD("691355959943233567", 10),
	ADMIN("691354891054219295", 1000),
	MODERATOR("691354383278932018", 100);
	
	private String id;
	private int index;
	
	private Rank(String id, int index)
	{
		this.id = id;
		this.index = index;
	}
	public String getId()
	{
		return this.id;
	}
	public int getIndex()
	{
		return this.index;
	}
	public static Rank getRankByIndex(int index)
	{
		return Rank.values()[index];
	}
}
