package fr.iiztp.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;

public class Functions
{
	public static double getPrice(String symbol) throws UnsupportedEncodingException, IOException
	{
		if(symbol.toLowerCase().equals("eursoc") || symbol.toLowerCase().equals("usdsoc"))
		{
			File trader = new File("traders/");
			double price = ( (getPeopleMoneyWithTrades() * (1322730097041.0 / (double)(Main.botTrader.getNbTrades())) / ((trader.list().length-1))))/100000;
			
			DecimalFormat df = new DecimalFormat("#.#####");
			df.setRoundingMode(RoundingMode.HALF_UP);
			
			if(symbol.toLowerCase().equals("eursoc"))
			{
				return Double.valueOf(df.format(price).replace(',', '.'));
			}
			
			if(symbol.toLowerCase().equals("usdsoc"))
			{
				return Double.valueOf(df.format(price/getPrice("eurusd")).replace(',', '.'));
			}
		}
		
		String price;
		String inputLine;
		String adress = "https://www.tbnewsite.fr/sockets/index.php?symbol=" + symbol.toUpperCase();
		
		if(symbol.equals("robot"))	adress = "https://www.tbnewsite.fr/sockets/index.php?symbol=robot";
		
		URL url = new URL(adress);

		try(BufferedReader stream = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8")))
		{
			price = stream.readLine();
			while ((inputLine = stream.readLine()) != null) price += "\n" + inputLine;
		}

		return (price != null) && (!price.isEmpty()) ? Double.valueOf(price) : -1.0;
	}
	public static void createJson(Object datas, String link, String fileName)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try
		{
			String json = gson.toJson(datas);

			File file = new File(link + fileName + ".json");
			
			if(!file.exists())
			{
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(json);
			bw.close();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static double getPeopleMoney()
	{
		File file = new File("traders/");
		
		double globalMoney = 0;
		
		for(File element : file.listFiles())
		{
			if(!element.getName().split(".json")[0].equals("690644015426699304"))
			{
				Trader user = Trader.getTrader(element.getName().split(".json")[0]);
				
				globalMoney += user.getBalance();
			}
		}
		return globalMoney;
	}
	
	public static double getPeopleMoneyWithTrades()
	{
		File file = new File("traders/");
		
		double globalMoney = 0;
		
		for(File element : file.listFiles())
		{
			if(!element.getName().split(".json")[0].equals("690644015426699304"))
			{
				Trader user = Trader.getTrader(element.getName().split(".json")[0]);
				for(int i = 0; i < user.getActiveTrades().size(); i++)
				{
					Trade activeTrade = user.getActiveTrades().get(i).getTrade();
					
					if(activeTrade != null)
					{
						globalMoney += activeTrade.getAmount();
					}
				}
				globalMoney += user.getBalance();
			}
		}
		
		return globalMoney;
	}
	
	public static Trader getBestTrader()
	{
		File file = new File("traders/");

		Trader bestPlayer = new Trader("ds");
		bestPlayer.setBalance(0);
		
		for(File element : file.listFiles())
		{
			Trader user = Trader.getTrader(element.getName().split(".json")[0]);
			
			if(!user.getId().equals("690644015426699304") && user.getBalance() >= bestPlayer.getBalance())
			{
				bestPlayer = user;
			}
		}
		
		return bestPlayer;
	}
	
	public static double round(double toRound)
	{
		return (double)Math.round(toRound*100)/100;
	}
}
