package fr.iiztp.tradebot;

import java.awt.Color;
import java.io.IOException;

import fr.iiztp.guild.Rank;
import fr.iiztp.languages.AvailableLanguages;
import fr.iiztp.languages.Languages;
import fr.iiztp.tradebot.commands.*;
import fr.iiztp.tradebot.commands.trade.*;
import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Listeners extends ListenerAdapter{
	@Override public void onMessageReceived(MessageReceivedEvent event)
	{
		Languages lang = AvailableLanguages.getLanguage(event.getTextChannel().getId());
		
		Message message = event.getMessage();
		if(message.getAuthor().isBot()) return;
		if(!message.getContentRaw().startsWith("!")) return;
		String command = message.getContentRaw().substring(1).split(" ")[0].toLowerCase();

		String args[] = null;
		if(message.getContentRaw().substring(command.length()).length() >= 2)
		{
			args = message.getContentRaw().substring(command.length()+2).split(" ");
		}

		Trader user = Trader.getTrader(message.getAuthor().getId());

		if(user.getPower() != null && (user.getPower().equals(Rank.ADMIN) || user.getPower().equals(Rank.MODERATOR)) && command.equals("ad"))
		{
			AdminCommands.callAdminCommands(event, args);
			if(event.getChannel().getId().equals("703301477417877544")) return;
			if(args != null && args.length >= 0 && !args[0].equals("nuke"))	event.getMessage().delete().complete();
			return;
		}
		if(user.getId().equals("224573432732319746"))
		{
			OwnerCommands.callOwnerCommands(event, args, command);
		}

		if(!message.getChannel().getId().equals(AvailableLanguages.getAvailableLanguages(lang).getTextChannelId())  && message.getChannelType().isGuild())
		{
			message.getChannel().sendMessage(lang.getToAvoidSpam().replace("{bot-commands}", Main.bot.getTextChannelById(AvailableLanguages.getAvailableLanguages(lang).getTextChannelId()).getAsMention())).complete();
			return;
		}

		switch(command)
		{
		case "price":	

			try
			{
				Price.callPrice(message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "balance":
		case "money":
		case "moulah":

			message.getChannel().sendMessage(lang.getShowBalance().replace("{money}", (double) Math.round(user.getBalance()*100)/100 + "")).complete();

			break;

		case "level":
		case "lvl":

			message.getChannel().sendMessage(lang.getShowLevel().replace("{level}", user.getLevel()+"")).complete();

			break;

		case "trade":

			try
			{
				Trade.callTrade(user, message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "alert":

			try
			{
				Alert.callAlert(user, message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "give":

			Give.callGive(event, args, user, lang);

			break;

		case "tradealert":

			try
			{
				TradeAlert.callTradeAlert(user, message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "active":

			try
			{
				Active.callActive(user, message, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "global":

			Global.callGlobal(message, lang);

			break;
		case "alerts":

			try
			{
				Alerts.callAlerts(user, message, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "close":

			try
			{
				Close.callClose(user, message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "ping":

			message.getChannel().sendMessage("Pong !\n").complete();

			break;

		case "mute":

			if(user.getMute())	user.setMute(false);
			else user.setMute(true);

			
			if(user.getMute())
			{
				message.getChannel().sendMessage(lang.getMute().replace("{mute}", "muted")).complete();
			}
			else
			{
				message.getChannel().sendMessage(lang.getMute().replace("{mute}", "unmuted")).complete();
			}

			break;

		case "sl":
		case "slp":

			try
			{
				Sl.callSl(user, message, args, command, lang);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}

			break;

		case "tp":
		case "tpp":

			try
			{
				Tp.callTp(user, message, args, command, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "stats":

			Stats.callStats(user, message, lang);

			break;

		case "top":

			Top.callTop(message);

			break;

		case "add":

			try
			{
				Add.callAdd(user, message, args, lang);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}

			break;

		case "help":

			Help.callHelp(user, message, lang);

			break;

		case "rules":

			break;

		case "jackpot":

			Jackpot.callJackpot(user, message, args, lang);

			break;
		default:	

		}
		user.testAchievement();
		Main.botTrader.saveUser();
	}

	@Override public void onGuildMemberJoin(GuildMemberJoinEvent event)
	{
		event.getGuild().getTextChannelById("691343648687915010").sendMessage(event.getMember().getAsMention() + " has joined Socialab ! Wish him the best !").complete();
		event.getGuild().getController().addSingleRoleToMember(event.getMember(), event.getGuild().getRoleById("690699942502727771")).complete();
		Trader newMember = new Trader(event.getUser().getId());
		newMember.saveUser();
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle("**Welcome aboard Socialab !** :smiley:", null);
		eb.setColor(Color.yellow);
		eb.setDescription("**SociaLab is a collaborative server oriented in trading charts !**");
		eb.addField("**How does it work ?**", "You begin with a 10Ⴥ account, and you speculate about the rise or the downs of the charts. Obviously, it is fake money.", false);
		eb.addField("**And when i don't have money anymore ?**", "A simple Ⴥ will be given to you everyday at 00:00, so take care of your money and don't take too high levers !", false);
		eb.addField("**About the server**", "Everytime you achieve something, you will evolve, have some bonuses and upgrade !", false);
		eb.addField("**So come on, come enjoy with us !**", "", false);
		eb.addField("__***Type !help to begin !***__", "", false);
		eb.addBlankField(true);
		try
		{
			event.getUser().openPrivateChannel().complete().sendMessage(eb.build()).complete();
		}
		catch(Exception e)
		{
			Main.bot.getTextChannelById("691343648687915010").sendMessage(event.getUser().getAsMention() + ", you need to know that i was unable to send you private message, without allowing me that, you won't receive the answers to the !help command and a notification everyday that you received a Ⴥ, also the privacy accorded to your trades will be increasingly reduced (when the autostops are engaged, this will be sent on a public channel on the guild).").complete();
		}
	}

	@Override public void onGuildMemberLeave(GuildMemberLeaveEvent event)
	{
		event.getGuild().getTextChannelById("691343648687915010").sendMessage(event.getMember().getAsMention() + " has left Socialab !").complete();
	}
}
