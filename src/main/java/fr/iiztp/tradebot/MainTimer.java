package fr.iiztp.tradebot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;
import java.util.TimerTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.iiztp.guild.Achievement;
import fr.iiztp.guild.CommandTimer;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.TradeAlert;
import fr.iiztp.utils.Functions;

public class MainTimer extends TimerTask{
	@Override public void run()
	{
		if(!String.valueOf((long)(Main.botTrader.getBalance() + Functions.getPeopleMoney() + Main.botTrader.getBestWin())).equals("1322730397041"))
		{
			Main.bot.getTextChannelById("691370207058591844").sendMessage(Main.bot.getUserById("224573432732319746").getAsMention() + ", there is a problem with the SCB ! Difference of : " + (1322730397041.0-Main.botTrader.getBalance() + Main.botTrader.getBestWin() + Functions.getPeopleMoney())).complete();
				
			Main.botTrader.setBalance(1322730397041.0 - (Main.botTrader.getBestWin() + Functions.getPeopleMoney()));
				
			Main.botTrader.saveUser();
				
			if(!String.valueOf((long)(Main.botTrader.getBalance() + Functions.getPeopleMoney() + Main.botTrader.getBestWin())).equals("1322730397041"))
				Main.bot.getTextChannelById("691370207058591844").sendMessage(Main.bot.getUserById("224573432732319746").getAsMention() + ", we tried to solve the problem but it's still here !").complete();
			else
				Main.bot.getTextChannelById("691370207058591844").sendMessage(Main.bot.getUserById("224573432732319746").getAsMention() + ", problem solved by ourselves !").complete();
		}
		
		int rnd = (int) (Math.random() * 100000);
		if(Math.random() <= 0.5)
		{
			rnd = -rnd;
		}
		Main.botTrader.setNbTrades(Main.botTrader.getNbTrades() + rnd); // On change la volat de l'eurosoc/usdsoc
		
		
		String json = "";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		File fileCommandTimers = new File("commandtimers/");
		for(File listFolders : fileCommandTimers.listFiles())
		{
			for(File folderCommands : listFolders.listFiles())
			{
				try(BufferedReader reader = new BufferedReader(new FileReader(folderCommands)))
				{
					String line = "";
					json = "";
					while ((line = reader.readLine()) != null)
					{
						json  += line;
					}
				}
			    catch(Exception e)
			    {
			    	e.printStackTrace();
			    }
				CommandTimer userTimer = gson.fromJson(json, CommandTimer.class);
				
				if(userTimer.getSecondsLeft() > 0)
				{
					userTimer.setSecondsLeft(userTimer.getSecondsLeft() - 10);
				
					userTimer.saveCommandTimer();
				}
				else folderCommands.delete();
			}
		}
		File file = new File("traders/");
		Trader user;
		Calendar calendar;
		for(File element : file.listFiles())
		{
			calendar = Calendar.getInstance();
			try(BufferedReader reader = new BufferedReader(new FileReader(element)))
			{
				String line = "";
				json = "";
				while ((line = reader.readLine()) != null)
				{
					json  += line;
				}
			}
		    catch(Exception e)
		    {
		    	e.printStackTrace();
		    }
			user = gson.fromJson(json, Trader.class);
			
			if(!user.getId().equals("690644015426699304"))
			{
				for(int i = 0; i < user.getActiveTrades().size(); i++)
				{
					try
					{
						if((user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getSl() != null && user.getActiveTrades().get(i).getTrade().isSL())
								|| (user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getTp() != null && user.getActiveTrades().get(i).getTrade().isTP())
								|| (user.getActiveTrades().get(i).getAlert() != null && user.getActiveTrades().get(i).getAlert().isAnAlert())
								|| (user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getEnterPrice() != 0.0 && user.getActiveTrades().get(i).getTrade().isBanqueroute()))
						{
							if(user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getEnterPrice() != 0.0 && user.getActiveTrades().get(i).getTrade().isBanqueroute())
							{
								try
								{
									Main.bot.getUserById(user.getId()).openPrivateChannel().complete().sendMessage("Your trade on `" + user.getTrade(i).getSymbol() + "` was closed, you lost `" + -user.getTrade(i).getAmount() + "`Ⴥ").complete();
								}
								catch(Exception e)
								{
									Main.bot.getGuildById("312631087480438794").getTextChannelById("691343908315332639").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + ", your trade on `" + user.getTrade(i).getSymbol() + "` was closed, you lost `" + -user.getTrade(i).getAmount() + "`Ⴥ").complete();
								}
								if(!user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE))
								{
									user.getAchievements().add(Achievement.MUST_CLOSE_TRADE);
									Achievement.MUST_CLOSE_TRADE.addAchievement(user);
								}
							}
							if(user.getActiveTrades().get(i).getAlert() != null && user.getActiveTrades().get(i).getAlert().isAnAlert())
							{
								TradeAlert activeTradeAlert = user.getTradeAlert(i);
								if(user.getActiveTrades().get(i).getTrade() != null)
								{
									user.getActiveTrades().get(i).getTrade().setEnterPrice(Functions.getPrice(user.getActiveTrades().get(i).getTrade().getSymbol()));
								}
								try
								{
									Main.bot.getUserById(user.getId()).openPrivateChannel().complete().sendMessage("Your alert on " + activeTradeAlert.getAlert().getSymbol() + " was triggered !").complete();
								}
								catch(Exception e)
								{
									Main.bot.getGuildById("312631087480438794").getTextChannelById("691343908315332639").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + ", your alert on " + activeTradeAlert.getAlert().getSymbol() + " was triggered !").complete();
								}
							}
								
							if(user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getTp() != null && user.getActiveTrades().get(i).getTrade().isTP())
							{
								try
								{
									Main.bot.getUserById(user.getId()).openPrivateChannel().complete().sendMessage("**TP** : Your trade on `" + user.getTrade(i).getSymbol() + "` was closed, with a p/l of `" + user.getTrade(i).getProfit() + "`Ⴥ").complete();
								}
								catch(Exception e)
								{
									Main.bot.getGuildById("312631087480438794").getTextChannelById("691343908315332639").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + ", **TP** : Your trade on " + user.getTrade(i).getSymbol() + " was closed, with a p/l of `\"" + user.getTrade(i).getProfit() + "\"`Ⴥ").complete();
								}
								if(!user.getAchievements().contains(Achievement.CLOSED_TAKEPROFIT))
								{
									user.getAchievements().add(Achievement.CLOSED_TAKEPROFIT);
									Achievement.CLOSED_TAKEPROFIT.addAchievement(user);
								}
							}
							if(user.getActiveTrades().get(i).getTrade() != null && user.getActiveTrades().get(i).getTrade().getSl() != null && user.getActiveTrades().get(i).getTrade().isSL())
							{
								try
								{
									Main.bot.getUserById(user.getId()).openPrivateChannel().complete().sendMessage("**SL** : Your trade on `" + user.getTrade(i).getSymbol() + "` was closed, with a p/l of `" + user.getTrade(i).getProfit() + "`Ⴥ").complete();
								}
								catch(Exception e)
								{
									Main.bot.getGuildById("312631087480438794").getTextChannelById("691343908315332639").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + ", **SL** : Your trade on " + user.getTrade(i).getSymbol() + "was closed, with a p/l of `" + user.getTrade(i).getProfit() + "`Ⴥ").complete();
								}
								if(!user.getAchievements().contains(Achievement.CLOSED_STOPLOSS))
								{
									user.getAchievements().add(Achievement.CLOSED_STOPLOSS);
									Achievement.CLOSED_STOPLOSS.addAchievement(user);
								}
							}
	
							user.close(i);
							user.saveUser();
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
				if(calendar.get(Calendar.HOUR_OF_DAY) == 0 && calendar.get(Calendar.MINUTE) == 0 && calendar.get(Calendar.SECOND) <= 10)
				{
					user.setBalance(user.getBalance() + 1);
					Main.botTrader.setBalance(Main.botTrader.getBalance() - 1);
					if(Main.bot.getUserById(user.getId()) != null)
					{
						try
						{
							if(!user.getMute())
							{
								Main.bot.getUserById(user.getId()).openPrivateChannel().complete().sendMessage("You won **1**Ⴥ for the day !").complete();
							}
						}
						catch(Exception e)
						{
							Main.bot.getGuildById("312631087480438794").getTextChannelById("691343908315332639").sendMessage(Main.bot.getUserById(user.getId()).getAsMention() + ", you received **1**Ⴥ for the day !").complete();
						}
					}
					user.saveUser();
				}
			}
		}
		Main.botTrader.saveUser();
	}
}
