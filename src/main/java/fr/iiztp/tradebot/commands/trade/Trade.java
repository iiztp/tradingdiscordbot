package fr.iiztp.tradebot.commands.trade;

import java.awt.Color;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Trade
{
	public static void callTrade(Trader user, Message message, String args[], Languages lang) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(args == null || !(args.length >= 0))
		{
			message.getChannel().sendMessage(lang.getChartArgument()).complete();
			return;
		}
		
		if(Functions.getPrice(args[0]) != -1)
		{
			message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			return;
		}
		
		if(!(args.length >= 1) || !(args[1].toLowerCase().equals("buy") || args[1].toLowerCase().equals("b") || args[1].toLowerCase().equals("sell") || args[1].toLowerCase().equals("s")))
		{
			message.getChannel().sendMessage(lang.getTradeException().replace("{exception}", lang.getBuyAndSell())).complete();
			return;
		}

		boolean buy;
		if(args[1].toLowerCase().equals("buy") || args[1].toLowerCase().equals("b"))	buy = true;
		else	buy = false;

		if(!(args.length >= 2))
		{
			message.getChannel().sendMessage(lang.getTradeException().replace("{exception}", lang.getAmount())).complete();
		}

		try
		{
			Double.valueOf(args[2]);
		}
		catch(Exception exception)
		{
			message.getChannel().sendMessage(lang.getInvalidAmount()).complete();
			return;
		}

		if(!(Double.valueOf(args[2]) <= Math.round(user.getBalance())))
		{
			message.getChannel().sendMessage(lang.getNotEnoughMoney()).complete();
			return;
		}

		if(!(Double.valueOf(args[2]) >= 5))
		{
			message.getChannel().sendMessage(lang.getGreaterThanFive()).complete();
			return;
		}

		if(!(args.length >= 3))
		{
			
			return;
		}
		
		int lever = 0;
		try
		{
			 lever = Integer.valueOf(args[3]);
		}
		catch(Exception exception)
		{
			
			return;
		}

		if(!(lever >= 1 && lever <= 200))
		{
			message.getChannel().sendMessage(lang.getLeverMustBe()).complete();
			return;
		}

		if(args[0].toLowerCase().equals("eursoc") || args[0].toLowerCase().equals("usdsoc"))
		{
			if(lever > 5)
			{
				lever = 5;
				message.getChannel().sendMessage(lang.getChangeLever().replace("{lever}", lever+"")).complete();
			}
		}

		user.setBalance(user.getBalance() - Integer.valueOf(args[2]));
		user.setNbTrades(user.getNbTrades() + 1);
		user.openTrade(args[0], buy, Functions.getPrice(args[0]), Integer.valueOf(args[2]), lever);
		if(Integer.valueOf(args[2]) > 100 && !user.getAchievements().contains(Achievement.HUNDRED_SOCIOLE_TRADE))
		{
			user.getAchievements().add(Achievement.HUNDRED_SOCIOLE_TRADE);
			Achievement.HUNDRED_SOCIOLE_TRADE.addAchievement(user);
		}
		if(Integer.valueOf(args[2]) > 1000 && !user.getAchievements().contains(Achievement.THOUSAND_SOCIOLE_TRADE))
		{
			user.getAchievements().add(Achievement.THOUSAND_SOCIOLE_TRADE);
			Achievement.THOUSAND_SOCIOLE_TRADE.addAchievement(user);
		}
		if(Integer.valueOf(args[2]) > 100000 && !user.getAchievements().contains(Achievement.HUNDRED_THOUSAND_SOCIOLE_TRADE))
		{
			user.getAchievements().add(Achievement.HUNDRED_THOUSAND_SOCIOLE_TRADE);
			Achievement.HUNDRED_THOUSAND_SOCIOLE_TRADE.addAchievement(user);
		}
		if(Integer.valueOf(args[2]) > 1000000 && !user.getAchievements().contains(Achievement.MILLION_SOCIOLE_TRADE))
		{
			user.getAchievements().add(Achievement.MILLION_SOCIOLE_TRADE);
			Achievement.MILLION_SOCIOLE_TRADE.addAchievement(user);
		}
		if(Integer.valueOf(args[2]) > 1000000000 && !user.getAchievements().contains(Achievement.BILLION_SOCIOLE_TRADE))
		{
			user.getAchievements().add(Achievement.BILLION_SOCIOLE_TRADE);
			Achievement.BILLION_SOCIOLE_TRADE.addAchievement(user);
		}
		user.saveUser();
		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(lang.getEmbedTrade().get("titleCreate"), null);
		if(user.getTrade(user.getActiveTrades().size() - 1).getBuy())
		{
			eb.setColor(Color.green);
			eb.setAuthor(lang.getEmbedTrade().get("bull"), null, "https://www.tbnewsite.fr/images/up.png");
		}
		else
		{
			eb.setColor(Color.red);
			eb.setAuthor(lang.getEmbedTrade().get("bear"), null, "https://www.tbnewsite.fr/images/down.png");
		}
		eb.setDescription(lang.getEmbedTrade().get("descriptionCreate").replace("{symbol}", args[0].toUpperCase()));
		eb.addField(lang.getEmbedTrade().get("symbol"), args[0].toUpperCase(), true);
		eb.addField(lang.getEmbedTrade().get("value"), String.valueOf(Functions.getPrice(args[0])), true);
		eb.addField(lang.getEmbedTrade().get("quantity"), args[2] + "Ⴥ", true);
		eb.addField(lang.getEmbedTrade().get("buysell"), args[1].toLowerCase(), true);
		eb.addField(lang.getEmbedTrade().get("lever"), lever + "", true);
		eb.addField(lang.getEmbedTrade().get("commission"), user.getTrade(user.getActiveTrades().size() - 1).getCommission() + "", true);
		eb.setFooter(lang.getAttention());
		eb.addBlankField(true);
		message.getChannel().sendMessage(eb.build()).complete();
	}
}
