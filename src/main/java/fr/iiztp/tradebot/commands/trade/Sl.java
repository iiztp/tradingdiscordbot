package fr.iiztp.tradebot.commands.trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;
import fr.iiztp.trader.tradealert.trade.TradeOrder;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.entities.Message;

public class Sl
{
	public static void callSl(Trader user, Message message, String args[], String command, Languages lang) throws UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE) && user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES) && (user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE) || user.getAchievements().contains(Achievement.TEN_TRADE)))) return;

		boolean isMarketPriceSL;
		if(command.equals("sl")) isMarketPriceSL = true;
		else isMarketPriceSL = false;
		
		if(args.length >= 0)
		{
			if(args.length >= 1)
			{
				int indexTrade = Integer.valueOf(args[0]);
				if(indexTrade < user.getActiveTrades().size())
				{
					Trade activeTrade = user.getTrade(indexTrade);
					double priceSL = Double.valueOf(args[1]);
					if(isMarketPriceSL)
					{
						if(activeTrade.getBuy() ?
								priceSL < Functions.getPrice(activeTrade.getSymbol())
								:priceSL > Functions.getPrice(activeTrade.getSymbol()))
						{
								activeTrade.setSl(new TradeOrder(priceSL, isMarketPriceSL));
								message.getChannel().sendMessage(lang.getSlTpRedefined().replace("{price}", priceSL+"").replace("{tpsl}", "SL")).complete();
						}
						else
						{
							if(priceSL < Functions.getPrice(activeTrade.getSymbol()))
							{
								message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "SL").replace("{lw}", "greater")).complete();
								return;
							}
							message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "SL").replace("{lw}", "lower")).complete();
							return;
						}
					}
					else
					{
						if(priceSL < activeTrade.getProfit())
						{
								activeTrade.setSl(new TradeOrder(priceSL, isMarketPriceSL));
								message.getChannel().sendMessage(lang.getSlpTppRedefined().replace("{price}", priceSL+"").replace("{tpsl}", "SL")).complete();
						}
						else
						{
							message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "SL").replace("{lw}", "lower")).complete();
							return;
						}
					}
					if(!user.getAchievements().contains(Achievement.DEFINE_STOPLOSS))
					{
						user.getAchievements().add(Achievement.DEFINE_STOPLOSS);
						Achievement.DEFINE_STOPLOSS.addAchievement(user);
					}
				}
				else	message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			}
			else	message.getChannel().sendMessage(lang.getInvalidPrice()).complete();
		}
		else	message.getChannel().sendMessage(lang.getNoIndex()).complete();
	}
}
