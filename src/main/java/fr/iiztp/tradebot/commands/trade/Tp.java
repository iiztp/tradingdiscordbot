package fr.iiztp.tradebot.commands.trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;
import fr.iiztp.trader.tradealert.trade.TradeOrder;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.entities.Message;

public class Tp
{
	public static void callTp(Trader user, Message message, String args[], String command, Languages lang) throws UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE) && user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES) && (user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE) || user.getAchievements().contains(Achievement.TEN_TRADE)))) return;

		boolean isMarketPriceTP;
		if(command.equals("tp")) isMarketPriceTP = true;
		else isMarketPriceTP = false;
		
		if(args.length >= 0)
		{
			if(args.length >= 1)
			{
				int indexTrade = Integer.valueOf(args[0]);
				if(indexTrade < user.getActiveTrades().size())
				{
					Trade activeTrade = user.getTrade(indexTrade);
					double priceTP = Double.valueOf(args[1]);
					if(isMarketPriceTP)
					{
						if(activeTrade.getBuy() ?
								priceTP > Functions.getPrice(activeTrade.getSymbol())
								:priceTP < Functions.getPrice(activeTrade.getSymbol()))
						{
								activeTrade.setTp(new TradeOrder(priceTP, isMarketPriceTP));
								message.getChannel().sendMessage(lang.getSlTpRedefined().replace("{tpsl}", "TP")).complete();
						}
						else
						{
							if(priceTP > Functions.getPrice(activeTrade.getSymbol()))
							{
								message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "TP").replace("{lw}", "lower")).complete();
								return;
							}
							message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "TP").replace("{lw}", "greater")).complete();
							return;
						}
					}
					else
					{
						if(priceTP > activeTrade.getProfit())
						{
								activeTrade.setTp(new TradeOrder(priceTP, isMarketPriceTP));
								message.getChannel().sendMessage(lang.getSlpTppRedefined().replace("{tpsl}", "TP")).complete();
						}
						else
						{
							message.getChannel().sendMessage(lang.getSlMustBe().replace("{tpsl}", "TP").replace("{lw}", "lower")).complete();
							return;
						}
					}
					if(!user.getAchievements().contains(Achievement.DEFINE_TAKEPROFIT))
					{
						user.getAchievements().add(Achievement.DEFINE_TAKEPROFIT);
						Achievement.DEFINE_TAKEPROFIT.addAchievement(user);
					}
				}
				else	message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			}
			else	message.getChannel().sendMessage(lang.getInvalidPrice()).complete();
		}
		else	message.getChannel().sendMessage(lang.getNoIndex()).complete();
	}
}
