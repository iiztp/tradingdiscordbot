package fr.iiztp.tradebot.commands.trade;

import java.awt.Color;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.languages.Languages;
import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.alert.Trigger;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class TradeAlert
{
	public static void callTradeAlert(Trader user, Message message, String[] args, Languages lang) throws UnsupportedEncodingException, IOException
	{

		if(!(args != null && args.length >= 0))
		{
			message.getChannel().sendMessage(lang.getChartArgument()).complete();
			return;
		}

		if(!(Functions.getPrice(args[0]) != -1))
		{
			message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			return;
		}
		if(!(args.length >= 1) || !(Trigger.getTrigger(args[1].toUpperCase()) != null))
		{
			message.getChannel().sendMessage(lang.getNoTrigger().replace("{links}", Main.bot.getTextChannelById("693548085355216906").getAsMention())).complete();
			return;
		}
		if(!(args.length >= 2))
		{
			message.getChannel().sendMessage(lang.getTradeException().replace("{exception}", lang.getPrice())).complete();
			return;
		}
		if(!(Double.valueOf(args[2]) != null))
		{
			message.getChannel().sendMessage(lang.getInvalidPrice()).complete();
			return;
		}

		if(!(args.length >= 3) || !(args[3].toLowerCase().equals("buy") || args[3].toLowerCase().equals("b") || args[3].toLowerCase().equals("sell") || args[3].toLowerCase().equals("s")))
		{
			message.getChannel().sendMessage(lang.getTradeException().replace("{exception}", lang.getBuyAndSell())).complete();
			return;
		}

		boolean buy;
		if(args[3].toLowerCase().equals("buy") || args[3].toLowerCase().equals("b"))	buy = true;
		else	buy = false;

		if(!(args.length >= 4))
		{
			message.getChannel().sendMessage(lang.getTradeException().replace("{exception}", lang.getAmount())).complete();
			return;
		}

		try
		{
			Integer.valueOf(args[4]);
		}
		catch(Exception exception)
		{	
			message.getChannel().sendMessage(lang.getInvalidAmount()).complete();
			return;
		}

		if(!(Double.valueOf(args[4]) <= user.getBalance()))
		{
			message.getChannel().sendMessage(lang.getNotEnoughMoney()).complete();
			return;
		}

		if(!(Integer.valueOf(args[4]) >= 5))
		{
			message.getChannel().sendMessage(lang.getGreaterThanFive()).complete();
			return;
		}

		int lever = 0;

		try
		{
			lever = Integer.valueOf(args[5]);
		}
		catch(Exception exception)
		{
			message.getChannel().sendMessage(lang.getLeverMustBe()).complete();
			return;
		}

		if(!(args.length >= 5) && !(lever >= 1 && lever <= 200))
		{
			message.getChannel().sendMessage(lang.getLeverMustBe()).complete();
			return;
		}
		
		user.openTradeAlert(args[0], buy, Integer.valueOf(args[4]), Integer.valueOf(args[5]), args[1], Double.valueOf(args[2]));
		user.saveUser();

		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(lang.getEmbedTrade().get("tradealertCreate"), null);
		if(user.getActiveTrades().get(user.getActiveTrades().size() - 1).getTrade().getBuy())
		{
			eb.setColor(Color.green);
			eb.setAuthor(lang.getEmbedTrade().get("bull"), null, "https://www.tbnewsite.fr/images/up.png");
		}
		else
		{
			eb.setColor(Color.red);
			eb.setAuthor(lang.getEmbedTrade().get("bear"), null, "https://www.tbnewsite.fr/images/down.png");
		}
		eb.setDescription(lang.getEmbedTrade().get("tradealertDescription").replace("{symbol}", args[0].toUpperCase()));
		eb.addField(lang.getEmbedTrade().get("symbol"), args[0].toUpperCase(), true);
		eb.addField(lang.getEmbedTrade().get("quantity"), args[4] + "Ⴥ", true);
		eb.addField(lang.getEmbedTrade().get("buysell"), args[3].toLowerCase(), true);
		eb.addField(lang.getEmbedTrade().get("lever"), args[5], true);
		eb.addField(lang.getEmbedTrade().get("tradealertTrigger"), args[1].toUpperCase() + " " + args[2], true);
		eb.setFooter(lang.getAttention());
		eb.addBlankField(true);
		message.getChannel().sendMessage(eb.build()).complete();
	}
}
