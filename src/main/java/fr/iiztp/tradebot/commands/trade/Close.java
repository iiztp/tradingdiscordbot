package fr.iiztp.tradebot.commands.trade;

import java.awt.Color;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.alert.Alert;
import fr.iiztp.trader.tradealert.trade.Trade;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Close
{
	public static void callClose(Trader user, Message message, String args[], Languages lang) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE) && user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES))) return;
		
		if(args != null && args.length >= 0)
		{
			if(Integer.valueOf(args[0]) >= 0 && Integer.valueOf(args[0]) < user.getActiveTrades().size())
			{
				Trade actualTrade = user.getTrade(Integer.valueOf(args[0]));
				Alert actualAlert = user.getAlert(Integer.valueOf(args[0]));
				if(actualTrade != null)
				{
					EmbedBuilder eb = new EmbedBuilder();
					eb.setTitle(lang.getTitleTrade(), null);
					eb.setDescription(lang.getDescriptionTrade());
					if(actualTrade.getProfit() >= 0)
					{
						eb.setColor(Color.green);
						eb.addField(lang.getProfitTrade(), (double)(Math.round((actualTrade.getProfit()*100))/100) + "Ⴥ", true);
					}
					else
					{
						eb.setColor(Color.red);
						eb.addField(lang.getLossTrade(), actualTrade.getProfit()  + "Ⴥ", true);
					}
					eb.setFooter(lang.getAttention());
					eb.addBlankField(true);
					message.getChannel().sendMessage(eb.build()).complete();
				}
				if(actualAlert != null)
				{
					EmbedBuilder eb = new EmbedBuilder();
					eb.setTitle(lang.getTitleAlert(), null);
					eb.setDescription(lang.getDescriptionAlert());
					eb.setColor(Color.blue);
					eb.setFooter(lang.getAttention());
					eb.addBlankField(true);
					message.getChannel().sendMessage(eb.build()).complete();
				}
				user.close(Integer.valueOf(args[0]));
				user.saveUser();
			}
			else	message.getChannel().sendMessage(lang.getUnknownChart()).complete();
		}
		else	message.getChannel().sendMessage(lang.getNoIndex()).complete();
	}
}
