package fr.iiztp.tradebot.commands.trade;

import java.awt.Color;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Active
{
	public static void callActive(Trader user, Message message, Languages lang) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE))) return;

		if(!user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES))
		{
			user.getAchievements().add(Achievement.SHOW_ACTIVE_TRADES);
			Achievement.SHOW_ACTIVE_TRADES.addAchievement(user);
		}
		
		List<Trade> trades = new ArrayList<>();
		
		for(int i = 0; i<user.getActiveTrades().size(); i++)
		{
			if(user.getTrade(i) != null && user.getTrade(i).getEnterPrice() != 0.0)
			{
				trades.add(user.getTrade(i));
			}
		}
		
		if(user.getActiveTrades() == null || user.getActiveTrades().size() <= 0 || trades == null || trades.isEmpty())
		{
			message.getChannel().sendMessage(lang.getNoActiveTrades()).complete();
		}
		else
		{
			for(int i = 0; i<user.getActiveTrades().size(); i++)
			{
				if(user.getTrade(i) != null)
				{
					message.getChannel().sendMessage("**Index "+i+"**").complete();
					
					Trade actualTrade = user.getTrade(i);
					String buySell;
					EmbedBuilder eb = new EmbedBuilder();
					double pl = actualTrade.getProfit();
					if(actualTrade.getBuy())
					{
						buySell = "Buy";
						eb.setAuthor("Bull Market to see !", null, "https://www.tbnewsite.fr/images/up.png");
					}
					else
					{
						buySell = "Sell";
						eb.setAuthor("Bear Market to see !", null, "https://www.tbnewsite.fr/images/down.png");
					}
					if(pl > 0)	eb.setColor(Color.green);
					else	eb.setColor(Color.red);
					
					String sign = "";
					if(actualTrade.getProfit() > 0)
					{
						sign += "+";
					}
					
					eb.setDescription(lang.getEmbedTrade().get("description").replace("{symbol}", actualTrade.getSymbol().toUpperCase()).replace("{profit}", pl + "").replace("{sign}", sign));
					eb.addField(lang.getEmbedTrade().get("symbol"), actualTrade.getSymbol().toUpperCase(), true);
					eb.addField(lang.getEmbedTrade().get("value"), String.valueOf(actualTrade.getEnterPrice()), true);
					eb.addField(lang.getEmbedTrade().get("valuenow"), String.valueOf(Functions.getPrice(actualTrade.getSymbol())), true);
					eb.addField(lang.getEmbedTrade().get("quantity"), String.valueOf(actualTrade.getAmount()) + "Ⴥ", true);
					eb.addField(lang.getEmbedTrade().get("buysell"), buySell, true);
					eb.addField(lang.getEmbedTrade().get("lever"), String.valueOf(actualTrade.getLever()), true);
					if(actualTrade.getTp() != null)
					{
						eb.addField("TP : ", String.valueOf(actualTrade.getTp().getPriceOrder()), true);
					}
					if(actualTrade.getSl() != null)
					{
						eb.addField("SL : ", String.valueOf(actualTrade.getSl().getPriceOrder()), true);
					}
					eb.addField(lang.getEmbedTrade().get("commission"), (Functions.round(actualTrade.getCommission())) + "Ⴥ", true);
					eb.setFooter(lang.getAttention());
					eb.addBlankField(true);

					message.getChannel().sendMessage(eb.build()).complete();
				}
			}
		}
	}
}
