package fr.iiztp.tradebot.commands.trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.entities.Message;

public class Add
{
	public static void callAdd(Trader user, Message message, String[] args, Languages lang) throws UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE) && user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES) && (user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE) || user.getAchievements().contains(Achievement.TEN_TRADE)) && user.getAchievements().contains(Achievement.REACH_THOUSAND_SOCIOLE))) return;
		
		if(args.length >= 0)
		{
			if(args.length >= 1)
			{
				int indexTrade = Integer.valueOf(args[0]);
				if(indexTrade < user.getActiveTrades().size())
				{
					Trade activeTrade = user.getTrade(indexTrade);
					int adds = Integer.valueOf(args[1]);
					if(adds <= user.getBalance())
					{
						activeTrade.setEnterPrice((((double)((double)activeTrade.getEnterPrice()*activeTrade.getAmount() + (double)(((double)Functions.getPrice(activeTrade.getSymbol())*adds))))/((double)(activeTrade.getAmount()+adds))));
						activeTrade.setAmount(activeTrade.getAmount() + adds);
						user.setBalance(user.getBalance() - adds);
						Main.botTrader.setBalance(Main.botTrader.getBalance() + adds);
						Main.botTrader.saveUser();
						message.getChannel().sendMessage(lang.getConfirmAdd().replace("{amount}", adds+"")).complete();
					}
					else	message.getChannel().sendMessage(lang.getNotEnoughMoney()).complete();
				}
				else	message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			}
			else	message.getChannel().sendMessage(lang.getAddNoAmount()).complete();
		}
		else	message.getChannel().sendMessage(lang.getNoIndex()).complete();
	}
}
