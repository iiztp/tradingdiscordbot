package fr.iiztp.tradebot.commands;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.languages.Languages;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.entities.Message;

public class Price
{
	public static void callPrice(Message message, String[] args, Languages lang) throws UnsupportedEncodingException, IOException
	{
		if(!(args != null && !args[0].isEmpty()))
		{
			message.getChannel().sendMessage(lang.getChartArgument()).complete();
			return;
		}
		
		if(Functions.getPrice(args[0]) != -1)	message.getChannel().sendMessage(lang.getPrice().replace("{symbol}", args[0].toUpperCase()).replace("{price}", Functions.getPrice(args[0].toUpperCase())+"")).complete();
		else	message.getChannel().sendMessage(lang.getUnknownChart()).complete();
	}
}
