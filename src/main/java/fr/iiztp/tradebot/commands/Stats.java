package fr.iiztp.tradebot.commands;

import java.awt.Color;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Stats
{
	public static void callStats(Trader user, Message message, Languages lang)
	{
		if(!(user.getAchievements().contains(Achievement.FIRST_TRADE) && user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES) && (user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE) || user.getAchievements().contains(Achievement.TEN_TRADE)))) return;

		EmbedBuilder eb = new EmbedBuilder();
		eb.setTitle(lang.getStats().get("title"), null);
		eb.setColor(Color.yellow);
		eb.setDescription(lang.getStats().get("description"));
		eb.addField(lang.getStats().get("field1"), String.valueOf(user.getNbTrades()), false);
		eb.addField(lang.getStats().get("field2"), String.valueOf(user.getSucceededTrades()), false);
		eb.addField(lang.getStats().get("field3"), String.valueOf(user.getFailedTrades()), false);
		eb.addField(lang.getStats().get("field4"), String.valueOf((((double)user.getSucceededTrades()/(double)user.getNbTrades()))*100) + " %", false);
		eb.addField(lang.getStats().get("field5"), String.valueOf(user.getActiveTrades().size()), false);
		eb.addField(lang.getStats().get("field6"), String.valueOf(user.getBestWin()) + " Ⴥ", false);
		eb.addField(lang.getStats().get("field7"), String.valueOf(user.getWorstLoss()) + " Ⴥ", false);
		eb.addField(lang.getStats().get("field8"), (double)((Math.round((user.getBalance()*100)/(double)(Functions.getPeopleMoney()))*100)/100) + " %", false);
		message.getChannel().sendMessage(eb.build()).complete();
	}
}
