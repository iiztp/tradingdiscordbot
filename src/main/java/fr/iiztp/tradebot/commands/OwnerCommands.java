package fr.iiztp.tradebot.commands;

import fr.iiztp.guild.Rank;
import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class OwnerCommands
{
	public static void callOwnerCommands(MessageReceivedEvent event, String[] args, String command)
	{
		if(event.getGuild().getMemberById(event.getAuthor().getId()).isOwner())
		{
			if(command.equals("ptn") || command.equals("demote"))
			{
				Member tagged = event.getMessage().getMentionedMembers().get(0);
				Trader taggedTrader = Trader.getTrader(tagged.getId());

				if(command.equals("ptn") && args.length >= 0 && args.length >= 1)
				{

					if(args[1].equals("modo"))
					{
						event.getGuild().getController().addSingleRoleToMember(tagged, event.getGuild().getRoleById("691354383278932018")).complete();
						taggedTrader.setPower(Rank.MODERATOR);
					}
					if(args[1].equals("admin"))
					{
						event.getGuild().getController().addSingleRoleToMember(tagged, event.getGuild().getRoleById("691354891054219295")).complete();
						taggedTrader.setPower(Rank.ADMIN);
					}
				}
				if(command.equals("demote"))
				{
					if(taggedTrader.getPower().equals(Rank.ADMIN))
					{
						event.getGuild().getController().removeSingleRoleFromMember(tagged, event.getGuild().getRoleById("691354891054219295")).complete();
						taggedTrader.setPower(null);
					}
					if(taggedTrader.getPower().equals(Rank.MODERATOR))
					{
						event.getGuild().getController().removeSingleRoleFromMember(tagged, event.getGuild().getRoleById("691354383278932018")).complete();
						taggedTrader.setPower(null);
					}
				}
				taggedTrader.saveUser();
				event.getMessage().delete().complete();
			}
		}
	}
}
