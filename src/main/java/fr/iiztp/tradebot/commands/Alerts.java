package fr.iiztp.tradebot.commands;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.entities.Message;
import fr.iiztp.trader.tradealert.alert.Alert;

public class Alerts
{
	public static void callAlerts(Trader user, Message message, Languages lang) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(!(user.getAchievements().contains(Achievement.ALERT_ME))) return;
		
		List<Alert> alerts = new ArrayList<>();

		if(user.getActiveTrades() == null || user.getActiveTrades().size() <= 0 || alerts == null || alerts.isEmpty())
		{
			message.getChannel().sendMessage(lang.getNoActiveAlerts()).complete();
			return;
		}
			
		for(int i = 0; i<user.getActiveTrades().size(); i++)
		{
			if(user.getAlert(i) != null)
			{
				message.getChannel().sendMessage("**Index " + i + "**").complete();
				message.getChannel().sendMessage(user.displayAlert(i).build()).complete();
				alerts.add(user.getAlert(i));
			}
		}
	}
}
