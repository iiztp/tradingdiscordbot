package fr.iiztp.tradebot.commands;

import java.awt.Color;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Help
{
	public static void callHelp(Trader user, Message message, Languages lang)
	{
		EmbedBuilder eb = new EmbedBuilder();
		
		eb = new EmbedBuilder();
		eb.setTitle(lang.getHelp().get("title"), null);
		eb.setColor(Color.yellow);
		eb.setDescription(lang.getHelp().get("description"));
		eb.addField("!price <symbol>", lang.getHelp().get("price").replace("{links}", Main.bot.getTextChannelById("693548085355216906").getAsMention()), false);
		eb.addField("!online", lang.getHelp().get("online"), false);
		eb.addField("!give <mentionnedTrader> <amount>", lang.getHelp().get("money"), false);
		eb.addField("!global", lang.getHelp().get("global"), false);
		eb.addField("!jackpot", lang.getHelp().get("jackpot"), false);
		eb.addField("!money/!balance", lang.getHelp().get("money"), false);
		eb.addField("!mute <on/off>", lang.getHelp().get("mute"), false);
		eb.addField("!trade <symbol> <buy/sell> <amount> <lever>", lang.getHelp().get("trade"), false);
		if(user.getAchievements().contains(Achievement.FIRST_TRADE))
		{
			eb.addField("!active", lang.getHelp().get("active"), false);
			if(user.getAchievements().contains(Achievement.SHOW_ACTIVE_TRADES))
			{
				eb.addField("!close <index>", lang.getHelp().get("close"), false);
				if(user.getAchievements().contains(Achievement.MUST_CLOSE_TRADE) || user.getAchievements().contains(Achievement.TEN_TRADE))
				{
					eb.addField("!sl <index> <price>", lang.getHelp().get("sl"), false);
					eb.addField("!tp <index> <price>", lang.getHelp().get("tp"), false);
					eb.addField("!slp <index> <amount>", lang.getHelp().get("slp"), false);
					eb.addField("!tpp <index> <amount>", lang.getHelp().get("tpp"), false);
					if(user.getAchievements().contains(Achievement.TEN_TRADE))
					{
						eb.addField("!stats", lang.getHelp().get("stats"), false);
						if(user.getAchievements().contains(Achievement.REACH_THOUSAND_SOCIOLE))
						{
							eb.addField("!add <index> <amount>", lang.getHelp().get("add"), false);
						}
					}
				}
			}
		}
		eb.setFooter(lang.getHelp().get("footer"));
		eb.addBlankField(true);
		try
		{
			message.getAuthor().openPrivateChannel().complete().sendMessage(eb.build()).complete();
		}
		catch(Exception e)
		{
			Main.bot.getTextChannelById("691343908315332639").sendMessage(message.getAuthor().getAsMention() + ", you need to know that i was unable to send you private message, without allowing me that, you won't receive the answers to the !help command and a notification everyday that you received a Ⴥ, also the privacy accorded to your trades will be increasingly reduced (when the autostops are engaged, this will be sent on a public channel on the guild).").complete();
		}
	}
}
