package fr.iiztp.tradebot.commands;

import java.awt.Color;
import java.io.File;

import fr.iiztp.trader.Trader;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Top
{
	public static void callTop(Message message)
	{
		EmbedBuilder eb = new EmbedBuilder();
		File file = new File("traders/");
		
		Trader user1 = new Trader("0");
		Trader user2 = new Trader("0");
		Trader user3 = new Trader("0");
		
		user1.setBalance(0.0);
		user2.setBalance(0.0);
		user3.setBalance(0.0);
		
		Trader test;
		Trader tmp;
		
		for(File element : file.listFiles())
		{
			if(element.getName().split(".json")[0].equals("690644015426699304")) continue;
			
			test = Trader.getTrader(element.getName().split(".json")[0]);
			
			tmp = test;
			
			if(test.getBalance() > user1.getBalance())
			{
				tmp = test;
				test = user1;
				user1 = tmp;
			}
			if(test.getBalance() > user2.getBalance())
			{
				tmp = test;
				test = user2;
				user2 = tmp;
			}
			if(test.getBalance() > user3.getBalance())
			{
				tmp = test;
				test = user3;
				user3 = tmp;
			}
		}
		eb = new EmbedBuilder();
		eb.setTitle("Here's the top : ", null);
		eb.setColor(Color.pink);
		eb.setDescription("The best of the best, the real, the true, the good ones are all here");
		eb.addField("First position", "**" + message.getGuild().getMemberById(user1.getId()).getEffectiveName().toString() + "** with a balance of **" + (double) Math.round(user1.getBalance()*100)/100 +"** Ⴥ", false);
		eb.addField("Second position", "**" + message.getGuild().getMemberById(user2.getId()).getEffectiveName().toString() + "** with a balance of **" + (double) Math.round(user2.getBalance()*100)/100 +"** Ⴥ", false);
		eb.addField("Third position", "**" + message.getGuild().getMemberById(user3.getId()).getEffectiveName().toString() + "** with a balance of **" + (double) Math.round(user3.getBalance()*100)/100 +"** Ⴥ", false);
		message.getChannel().sendMessage(eb.build()).complete();
	}
}
