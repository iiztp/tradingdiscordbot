package fr.iiztp.tradebot.commands;

import java.awt.Color;
import java.io.File;
import java.util.Collection;
import java.util.List;

import fr.iiztp.guild.Achievement;
import fr.iiztp.guild.Rank;
import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.trade.Trade;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class AdminCommands
{
	public static void callAdminCommands(MessageReceivedEvent event, String[] args)
	{
		Rank userPower = Trader.getTrader(event.getAuthor().getId()).getPower();
		List<Member> mentionnedUsers = event.getMessage().getMentionedMembers();
		
		if(args == null || args.length <= 0)	return;
		
		if(args[0].equals("help"))
		{
			if(!event.getChannel().getId().equals("703301477417877544")) return;
			
			EmbedBuilder eb = new EmbedBuilder();
			
			eb = new EmbedBuilder();
			eb.setTitle("Here's the admin help : ", null);
			eb.setColor(Color.gray);
			eb.setDescription("Note that you have here all the commands of the admin/modos, even if you have no access to it (sometimes)");
			eb.addField("!ad deltimer <commandToDelTimer> <TraderMentionned>", "Deletes the timer of a command", false);
			eb.addField("!ad give <TraderMentionned> <amount>", "Gives money to the user you mentionned without taking it from your balance", false);
			eb.addField("!ad see <TraderMentionned> :OptionnalIndex:", "Shows you the balance, the stats and the trades if you put an index", false);
			eb.addField("!ad add/del <TraderMentionned> <Achievement>", "Adds/Deletes the achievements you want avaialble in the channel" + Main.bot.getTextChannelById("703994729507061771").getAsMention(), false);
			eb.addField("!ad nuke <nbOfMessages>", "Deletes the number of messages you want in a channel (up to a hundred not included)", false);
			eb.addBlankField(true);
			
			event.getChannel().sendMessage(eb.build()).complete();
		}
		
		if(args[0].equals("give"))
		{
			if(mentionnedUsers != null && !mentionnedUsers.isEmpty() && mentionnedUsers.size() >= 0 && args.length > 1)
			{
				Trader mentionnedTrader = Trader.getTrader(mentionnedUsers.get(0).getId());
					
				int amount = 0;
					
				try
				{
					amount = Integer.valueOf(args[2]);
				}
				catch(Exception exception)
				{
					try
					{
						amount = Integer.valueOf(args[1]);
					}
					catch(Exception exception2)
					{
						return;
					}
				}
					
				mentionnedTrader.setBalance(mentionnedTrader.getBalance() + amount);
				mentionnedTrader.saveUser();
				
				Main.botTrader.setBalance(Main.botTrader.getBalance() - amount);
				Main.botTrader.saveUser();
					
				Main.bot.getTextChannelById("702956906188374077").sendMessage(event.getAuthor().getAsMention() + " gave **" + amount + "**Ⴥ to " + mentionnedUsers.get(0).getAsMention()).complete();
					
				try
				{
					mentionnedUsers.get(0).getUser().openPrivateChannel().complete().sendMessage(event.getAuthor().getAsMention() + " gave you **" + amount + "**Ⴥ").complete();
				}
				catch(Exception exception)
				{
					Main.bot.getTextChannelById("691343908315332639").sendMessage(event.getAuthor().getAsMention() + " gave **" + amount + "**Ⴥ to " + mentionnedUsers.get(0).getAsMention()).complete();
				}
			}
			else return;
		}
		
		if(args[0].equals("achievement"))
		{
			if(mentionnedUsers == null)		return;
			if(args.length < 4) return;
			
			int index = -1;
			
			for(int i = 0; i < Achievement.values().length; i++)
			{
				if(Achievement.values()[i].toString().equals(args[3].toUpperCase()))
				{
					index = i;
					break;
				}
			}
			if(index == -1) return;
			
			Trader mentionnedTrader = Trader.getTrader(mentionnedUsers.get(0).getId());
			
			if(args[2].equals("add"))
			{
				mentionnedTrader.getAchievements().add(Achievement.values()[index]);
				Achievement.values()[index].addAchievement(mentionnedTrader);
			}
			if(args[2].equals("del"))
			{
				mentionnedTrader.getAchievements().remove(Achievement.values()[index]);
				Achievement.values()[index].deleteAchievement(mentionnedTrader);
			}
			mentionnedTrader.saveUser();
			return;
		}
		
		if(args[0].equals("see"))
		{
			if(mentionnedUsers == null)		return;
			
			Trader mentionnedTrader = Trader.getTrader(mentionnedUsers.get(0).getId());
			if(args.length <= 2)
			{
				EmbedBuilder eb = new EmbedBuilder();
				eb.setTitle("Menu of the trader " + mentionnedUsers.get(0).getEffectiveName() + " : ", null);
				eb.setColor(Color.gray);
				eb.setDescription(mentionnedUsers.get(0).getEffectiveName() + " is " + mentionnedTrader.getRank().toString());
				
				eb.addField("Money : ", mentionnedTrader.getBalance() + " Ⴥ", false);
				eb.addField("Number of active trades/alerts : ", mentionnedTrader.getActiveTrades().size() + "", false);
				eb.addField("Number of total trades : ", mentionnedTrader.getNbTrades() + "", false);
				eb.addField("Ratio (in %)", String.valueOf((((double)mentionnedTrader.getSucceededTrades()/(double)mentionnedTrader.getNbTrades()))*100) + " %", false);
				eb.addField("Best amount won by trading", mentionnedTrader.getBestWin() + " Ⴥ", false);
				eb.addField("Worst amount lost by trading", mentionnedTrader.getWorstLoss() + " Ⴥ", false);
				eb.addField("Ownings on the server (in %)", (double)((Math.round((mentionnedTrader.getBalance()*100)/(double)(Functions.getPeopleMoney()))*100)/100) + " %", false);
				
				Main.bot.getTextChannelById("703301477417877544").sendMessage(eb.build()).complete();
				return;
			}
			else
			{
				if(args[2].isEmpty()) return;
				try
				{
					Integer.valueOf(args[2]);
				}
				catch(Exception e)
				{
					return;
				}
				
				if(Integer.valueOf(args[2]) >= mentionnedTrader.getActiveTrades().size() || Integer.valueOf(args[2]) < 0)	return;
				
				try
				{
					Main.bot.getTextChannelById("703301477417877544").sendMessage("View of the index " + args[2] + " of the trader " + mentionnedUsers.get(0).getEffectiveName()).complete();
					if(mentionnedTrader.getActiveTrades().get(Integer.valueOf(args[2])).getTrade() != null)
					{
						Trade actualTrade = mentionnedTrader.getTrade(Integer.valueOf(args[2]));
						String buySell;
						String description = "Your trade on **" + actualTrade.getSymbol().toUpperCase() + "** : **";
						EmbedBuilder eb = new EmbedBuilder();
						double pl = actualTrade.getProfit();
						if(actualTrade.getBuy())
						{
							buySell = "Buy";
							eb.setAuthor("Bull Market to see !", null, "https://www.tbnewsite.fr/images/up.png");
						}
						else
						{
							buySell = "Sell";
							eb.setAuthor("Bear Market to see !", null, "https://www.tbnewsite.fr/images/down.png");
						}
						if(pl > 0)
						{
							eb.setColor(Color.green);
							description += "+";
						}
						else
						{
							eb.setColor(Color.red);
						}
						description += String.valueOf((double)Math.round(pl*100)/100);
						description += "**Ⴥ";
						eb.setDescription(description);
						eb.addField("Symbol : ", actualTrade.getSymbol().toUpperCase(), true);
						eb.addField("Value (at time of buy) : ", String.valueOf(actualTrade.getEnterPrice()), true);
						eb.addField("Value now : ", String.valueOf(Functions.getPrice(actualTrade.getSymbol())), true);
						eb.addField("Quantity : ", String.valueOf(actualTrade.getAmount()) + "Ⴥ", true);
						eb.addField("Buy/Sell : ", buySell, true);
						eb.addField("Multiplier/Lever : ", String.valueOf(actualTrade.getLever()), true);
						if(actualTrade.getTp() != null)
						{
							eb.addField("Take profit : ", String.valueOf(actualTrade.getTp().getPriceOrder()), true);
						}
						if(actualTrade.getSl() != null)
						{
							eb.addField("Stop Loss : ", String.valueOf(actualTrade.getSl().getPriceOrder()), true);
						}
						eb.addField("Commission taken : ", (Functions.round(actualTrade.getCommission())) + "Ⴥ", true);
						eb.setFooter("Pay attention while trading, it is at your peril");
						eb.addBlankField(true);
						
						Main.bot.getTextChannelById("703301477417877544").sendMessage(eb.build()).complete();
					}
					if(mentionnedTrader.getActiveTrades().get(Integer.valueOf(args[2])).getAlert() != null)
					{
						Main.bot.getTextChannelById("703301477417877544").sendMessage(mentionnedTrader.getActiveTrades().get(Integer.valueOf(args[2])).getAlert().displayAlert().build()).complete();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				
				return;
			}
		}
		
		if(args[0].equals("deltimer"))
		{
			if(args.length >= 2)
			{
				if(mentionnedUsers != null && !mentionnedUsers.isEmpty() && mentionnedUsers.size() >= 0)
				{
					try
					{
						File delFile = new File("commandtimers/" + args[1] + "/" + mentionnedUsers.get(0).getId() + ".json");
						delFile.delete();
						
						event.getMessage().getChannel().sendMessage(mentionnedUsers.get(0).getAsMention() + " your timer has been **disabled** on the command **" + args[1] + "** (by" + event.getAuthor().getAsMention() + ")").complete();
					}
					catch(Exception exception)
					{
						exception.printStackTrace();
						return;
					}
				}
				else return;
			}
			else	return;
		}
		
		if(userPower == Rank.ADMIN)
		{
			if(args.length >= 0 && args[0].equals("nuke"))
			{
				if(args.length >= 1 && !args[1].isEmpty())
				{
					try
					{
						if(!(Integer.valueOf(args[1]) >= 100 || Integer.valueOf(args[1]) <= 0))	 throw new Exception();
					}
					catch(Exception e)
					{
						return;
					}
					Collection<Message> nb = event.getChannel().getHistory().retrievePast((int)(Integer.valueOf(args[1]) + 1)).complete();
					event.getTextChannel().deleteMessages(nb).complete();
				}
				else 
				{
					event.getMessage().getChannel().sendMessage("You must put the number of messages to delete").complete();
				}
			}
		}
		
	}
}
