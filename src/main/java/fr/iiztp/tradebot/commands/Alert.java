package fr.iiztp.tradebot.commands;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import fr.iiztp.guild.Achievement;
import fr.iiztp.languages.Languages;
import fr.iiztp.trader.Trader;
import fr.iiztp.trader.tradealert.alert.Trigger;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.entities.Message;

public class Alert
{
	public static void callAlert(Trader user, Message message, String[] args, Languages lang) throws UnsupportedEncodingException, IOException
	{	
		if(!(args != null && args.length >= 0))
		{
			message.getChannel().sendMessage(lang.getChartArgument()).complete();
			return;
		}
		if(!(Functions.getPrice(args[0]) != -1))
		{
			message.getChannel().sendMessage(lang.getUnknownChart()).complete();
			return;
		}
		if(!(args.length >= 1) || !(Trigger.getTrigger(args[1].toUpperCase()) != null))
		{
			message.getChannel().sendMessage(lang.getNoTrigger().replace("{links}", message.getGuild().getTextChannelById("693548085355216906").getAsMention())).complete();
			return;
		}	
		if(!(args.length >= 2) || !(Double.valueOf(args[2]) != null))
		{
			message.getChannel().sendMessage(lang.getInvalidPrice()).complete();
			return;
		}
		
		if(!user.getAchievements().contains(Achievement.ALERT_ME))
		{
			user.getAchievements().add(Achievement.ALERT_ME);
			Achievement.ALERT_ME.addAchievement(user);
		}
		user.openAlert(args[0], Double.valueOf(args[2]), args[1]);
		user.saveUser();

		message.getChannel().sendMessage(lang.getOpenAlert().replace("{symbol}", args[0])).complete();
		message.getChannel().sendMessage(user.displayAlert(user.getActiveTrades().size()-1).build()).complete();
	}
}
