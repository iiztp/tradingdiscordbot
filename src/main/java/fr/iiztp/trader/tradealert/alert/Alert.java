package fr.iiztp.trader.tradealert.alert;

import java.awt.Color;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import fr.iiztp.utils.Functions;
import net.dv8tion.jda.api.EmbedBuilder;

public class Alert
{
	private String symbol;
	private double price;
	private Trigger trigger;
	
	public Alert(String symb, double p, String trig)
	{
		this.symbol = symb;
		this.price = p;
		this.trigger = Trigger.getTrigger(trig.toUpperCase());
	}
	public String getSymbol()
	{
		return this.symbol;
	}
	public double getPrice()
	{
		return this.price;
	}
	public Trigger getTrigger()
	{
		return this.trigger;
	}
	public boolean isAnAlert() throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this != null)
		{
			double actualPrice = Double.valueOf(Functions.getPrice(this.symbol));
			if(this.getTrigger().equals(Trigger.GO_UPPER))
			{
				if(this.getPrice() <= actualPrice)
				{
					return true;
				}
			}
			if(this.getTrigger().equals(Trigger.GO_BELOW))
			{
				if(this.getPrice() >= actualPrice)
				{
					return true;
				}
			}
		}
		return false;
	}
	public EmbedBuilder displayAlert() throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		EmbedBuilder eb = new EmbedBuilder();

		eb.setColor(Color.blue);
		String description = "Your alert on " + this.getSymbol();
		eb.setDescription(description);
		eb.addField("Symbol : ", this.symbol.toUpperCase(), true);
		eb.addField("Actual value : ", String.valueOf(Functions.getPrice(this.getSymbol())), true);
		eb.addField("Trigger : ", this.getTrigger().getName(), true);
		eb.addField("Goal : ", String.valueOf(this.getPrice()), true);
		eb.setFooter("Pay attention while trading, it is at your peril");
		eb.addBlankField(true);
		
		return eb;
	}
}
