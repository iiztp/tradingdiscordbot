package fr.iiztp.trader.tradealert.alert;

public enum Trigger
{
	GO_UPPER("GO_UPPER"),
	GO_BELOW("GO_BELOW");
	
	private String name;
	
	private Trigger(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public static Trigger getTrigger(String name)
	{
		for(Trigger element : values())
		{
			if(element.getName().equals(name))
			{
				return element;
			}
		}
		return null;
	}
}
