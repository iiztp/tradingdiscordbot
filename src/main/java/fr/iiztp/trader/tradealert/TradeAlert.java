package fr.iiztp.trader.tradealert;

import fr.iiztp.trader.tradealert.alert.Alert;
import fr.iiztp.trader.tradealert.trade.Trade;

public class TradeAlert
{
	private Alert alert = null;
	private Trade trade = null;
	
	public TradeAlert(Alert alert, Trade trade)
	{
		this.alert = alert;
		this.trade = trade;
	}
	public Trade getTrade()
	{
		return this.trade;
	}
	public Alert getAlert()
	{
		return this.alert;
	}
	public void setAlert(Alert alert)
	{
		this.alert = alert;
	}
	public void setTrade(Trade trade) {
		this.trade = trade;
	}
}
