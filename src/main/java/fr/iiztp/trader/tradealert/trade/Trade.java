package fr.iiztp.trader.tradealert.trade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ThreadLocalRandom;

import fr.iiztp.tradebot.Main;
import fr.iiztp.utils.Functions;



public class Trade
{
	private String symbol;
	private double enterPrice;
	private boolean buy;
	private int amount;
	private int lever;
	TradeOrder tp = null;
	TradeOrder sl = null;
	private double commission;
	
	public Trade(String symbol, boolean buy, double price, int amount, int lever)
	{
		this.symbol = symbol;
		this.buy = buy;
		
		this.commission = -amount + amount*(1 + (ThreadLocalRandom.current().nextDouble(0.00080000, 0.00090000))*lever);
		
		this.enterPrice = price;
		
		this.amount = amount;
		this.lever = lever;
		
		Main.botTrader.setBalance(Main.botTrader.getBalance() + this.getAmount());
		
		Main.botTrader.saveUser();
	}
	public double getEnterPrice()
	{
		return this.enterPrice;
	}
	public boolean getBuy()
	{
		return this.buy;
	}
	public int getAmount()
	{
		return this.amount;
	}
	public double getCommission()
	{
		return commission;
	}
	public int getLever()
	{
		return this.lever;
	}
	public String getSymbol()
	{
		return this.symbol;
	}
	public void setEnterPrice(double enterPrice)
	{
		this.enterPrice = enterPrice;
	}
	public void setAmount(int amount)
	{
		this.amount = amount;
	}
	public void setSl(TradeOrder sl)
	{
		this.sl = sl;
	}
	public TradeOrder getSl()
	{
		return sl;
	}
	public void setTp(TradeOrder tp)
	{
		this.tp = tp;
	}
	public TradeOrder getTp()
	{
		return tp;
	}
	public boolean isBanqueroute() throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this.getProfit() + this.getAmount() <= 0)
		{
			return true;
		}
		return false;
	}
	public boolean isSL() throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this.getSl() != null)
		{
			if(this.getSl().getIsMarketPrice())
			{
				if(this.getBuy() ?
						Functions.getPrice(this.getSymbol()) <= this.getSl().getPriceOrder()
						:Functions.getPrice(this.getSymbol()) >= this.getSl().getPriceOrder())
							return true;
			}
			else
			{
				if(this.getProfit() <= this.getSl().getPriceOrder())
					return true;
			}
		}
		return false;
	}
	public boolean isTP() throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this.getTp() != null)
		{
			if(this.getTp().getIsMarketPrice())
			{
				if(this.getBuy() ?
						Functions.getPrice(this.getSymbol()) >= this.getTp().getPriceOrder()
						:Functions.getPrice(this.getSymbol()) <= this.getTp().getPriceOrder())
					return true;
			}
			else
			{
				if(this.getProfit() >= this.getTp().getPriceOrder())
					return true;
			}
		}
		return false;
	}
	public double getProfit() throws UnsupportedEncodingException, IOException
	{
		double profit;
		
		if(this.getEnterPrice() == 0.0)	return 0.0;
		
		if(this.buy)
		{
			profit = (double) ((Math.round((((this.getLever()*this.getAmount()*Functions.getPrice(this.getSymbol()))/this.getEnterPrice()) - this.getAmount()*this.getLever()) * 100)/100)- (double)Math.round(this.getCommission()*100)/100);
		}
		else
		{
			profit = (double) ((Math.round(-(((this.getLever()*this.getAmount()*Functions.getPrice(this.getSymbol()))/this.getEnterPrice()) - this.getAmount()*this.getLever()) * 100)/100)- (double)Math.round(this.getCommission()*100)/100);
		}
		return profit;
	}
}
