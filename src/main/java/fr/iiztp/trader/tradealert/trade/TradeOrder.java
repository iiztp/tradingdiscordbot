package fr.iiztp.trader.tradealert.trade;

public class TradeOrder
{
	private double priceOrder;
	private boolean isMarketPrice;
	
	public TradeOrder(double order, boolean marketPrice)
	{
		this.priceOrder = order;
		this.isMarketPrice = marketPrice;
	}
	
	public double getPriceOrder()
	{
		return this.priceOrder;
	}
	public void setPriceOrder(double order)
	{
		this.priceOrder = order;
	}
	
	public boolean getIsMarketPrice()
	{
		return this.isMarketPrice;
	}
	public void setMarketPrice(boolean isMarketPrice)
	{
		this.isMarketPrice = isMarketPrice;
	}
}
