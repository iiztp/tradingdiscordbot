package fr.iiztp.trader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.iiztp.guild.Achievement;
import fr.iiztp.guild.Rank;
import fr.iiztp.languages.AvailableLanguages;
import fr.iiztp.tradebot.Main;
import fr.iiztp.trader.tradealert.TradeAlert;
import fr.iiztp.trader.tradealert.alert.Alert;
import fr.iiztp.trader.tradealert.trade.Trade;
import net.dv8tion.jda.api.EmbedBuilder;


public class Trader 
{
	private static Map<Integer, Achievement> nbTradesAchievements = new HashMap<>();
	static
	{
		nbTradesAchievements.put(1, Achievement.FIRST_TRADE);
	}
	
	private String id;
	private double balance = 10.0;
	private int succeededTrades = 0;
	private int failedTrades = 0;
	private int nbTrades = 0;
	private int level = 1;
	private double bestWin = 0;
	private double worstLoss = 0;
	private Rank rank = Rank.DEFAULT;
	private Rank power = null;
	private List<TradeAlert> activeTrades = new ArrayList<>();
	private List<Achievement> achievements = new ArrayList<>();
	private List<AvailableLanguages> langs = new ArrayList<>();
	private boolean mute = false;
	
	public Trader(String idMember)
	{
		this.id = idMember;
	}
	public void setMute(boolean m)
	{
		this.mute = m;
	}
	public boolean getMute()
	{
		return this.mute;
	}
	public String getId()
	{
		return this.id;
	}
	public double getBalance()
	{
		return this.balance;
	}
	public void setLangs(List<AvailableLanguages> langs) {
		this.langs = langs;
	}
	public List<AvailableLanguages> getLangs() {
		return langs;
	}
	public int getSucceededTrades()
	{
		return this.succeededTrades;
	}
	public int getFailedTrades()
	{
		return this.failedTrades;
	}
	public int getNbTrades()
	{
		return this.nbTrades;
	}
	public int getLevel()
	{
		return this.level;
	}
	public Trade getTrade(int index)
	{
		if(this.getActiveTrades().get(index).getTrade() != null)
		{
			return this.getActiveTrades().get(index).getTrade();
		}
		return null;
	}
	public Alert getAlert(int index)
	{
		if(this.getActiveTrades().get(index).getAlert() != null)
		{
			return this.getActiveTrades().get(index).getAlert();
		}
		return null;
	}
	public TradeAlert getTradeAlert(int index)
	{
		return this.activeTrades.get(index);
	}
	public Rank getRank()
	{
		return this.rank;
	}
	public void setRank(Rank e)
	{
		this.rank = e;
	}
	public List<Achievement> getAchievements()
	{
		return this.achievements;
	}
	public void setBalance(double money)
	{
		this.balance = money;
	}
	public void setSucceededTrades(int suc)
	{
		this.succeededTrades = suc;
	}
	public Rank getPower()
	{
		return this.power;
	}
	public void setPower(Rank e)
	{
		this.power = e;
	}
	public List<TradeAlert> getActiveTrades()
	{
		return activeTrades;
	}
	public void setFailedTrades(int fail)
	{
		this.failedTrades = fail;
	}
	public void setNbTrades(int nb)
	{
		this.nbTrades = nb;
	}
	public void setLevel(int nb)
	{
		this.level = nb;
	}
	public void openTrade(String symbol, boolean buy, double price, int amount, int lever)
	{
		this.activeTrades.add(new TradeAlert(null, new Trade(symbol, buy, price, amount, lever)));
	}
	public void openAlert(String symbol, double price, String trigger)
	{
		this.activeTrades.add(new TradeAlert(new Alert(symbol, price, trigger), null));
	}
	public void openTradeAlert(String symbol, boolean buy, int amount, int lever, String trigger, double priceExpected)
	{
		this.activeTrades.add(new TradeAlert(new Alert(symbol, priceExpected, trigger), new Trade(symbol, buy, 0.0, amount, lever)));
	}
	public double getWorstLoss()
	{
		return this.worstLoss;
	}
	public void setWorstLoss(double wl)
	{
		this.worstLoss = wl;
	}
	public double getBestWin()
	{
		return this.bestWin;
	}
	public void setBestWin(double bw)
	{
		this.bestWin = bw;
	}
	private void closeAlert(int index)
	{
		if(this.getActiveTrades().size() > index && this.getAlert(index) != null)
		{
			this.getActiveTrades().get(index).setAlert(null);
		}
		this.saveUser();
	}
	private void closeTrade(int index) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this.getActiveTrades().size() < index || this.getTrade(index) == null)	return;
		
		Trade actualTrade = this.getTrade(index);
		
		double profit = actualTrade.getProfit();
		
		if(actualTrade.getEnterPrice() != 0.0 && (profit + actualTrade.getAmount()) > 0)
		{
			this.setBalance((double)((double)(this.getBalance() + profit) + actualTrade.getAmount()));
			
			Main.botTrader.setBalance((double)((double)Main.botTrader.getBalance() - profit) - actualTrade.getAmount());
		}
		
		if(profit <= 0)
		{
			this.setFailedTrades(this.getFailedTrades() + 1);
			Main.botTrader.setFailedTrades(Main.botTrader.getFailedTrades() + 1);
		}
		else
		{
			this.setSucceededTrades(this.getSucceededTrades() + 1);
			Main.botTrader.setSucceededTrades(Main.botTrader.getSucceededTrades() + 1);
		}
		
		Main.botTrader.saveUser();
		
		if(this.getWorstLoss() >= profit)	this.setWorstLoss(profit);
		if(this.getBestWin() <= profit)		this.setBestWin(profit);
		
		if(profit > 100 && !this.getAchievements().contains(Achievement.WIN_HUNDRED_SOCIOLES))
		{
			this.achievements.add(Achievement.WIN_HUNDRED_SOCIOLES);
			Achievement.WIN_HUNDRED_SOCIOLES.addAchievement(this);
		}
		if(profit > 1000 && !this.getAchievements().contains(Achievement.WIN_THOUSAND_SOCIOLES))
		{
			this.achievements.add(Achievement.WIN_THOUSAND_SOCIOLES);
			Achievement.WIN_THOUSAND_SOCIOLES.addAchievement(this);
		}
		if(profit > 100000 && !this.getAchievements().contains(Achievement.WIN_HUNDRED_THOUSAND_SOCIOLES))
		{
			this.achievements.add(Achievement.WIN_HUNDRED_THOUSAND_SOCIOLES);
			Achievement.WIN_HUNDRED_THOUSAND_SOCIOLES.addAchievement(this);
		}
		if(profit > 1000000 && !this.getAchievements().contains(Achievement.WIN_MILLION_SOCIOLES))
		{
			this.achievements.add(Achievement.WIN_MILLION_SOCIOLES);
			Achievement.WIN_MILLION_SOCIOLES.addAchievement(this);
		}
		if(profit > 1000000000 && !this.getAchievements().contains(Achievement.WIN_BILLION_SOCIOLES))
		{
			this.achievements.add(Achievement.WIN_BILLION_SOCIOLES);
			Achievement.WIN_BILLION_SOCIOLES.addAchievement(this);
		}
		if(profit < 100 && !this.getAchievements().contains(Achievement.LOSE_HUNDRED_SOCIOLES))
		{
			this.achievements.add(Achievement.LOSE_HUNDRED_SOCIOLES);
			Achievement.LOSE_HUNDRED_SOCIOLES.addAchievement(this);
		}
		if(profit < -1000 && !this.getAchievements().contains(Achievement.LOSE_THOUSAND_SOCIOLES))
		{
			this.achievements.add(Achievement.LOSE_THOUSAND_SOCIOLES);
			Achievement.LOSE_THOUSAND_SOCIOLES.addAchievement(this);
		}
		if(profit < -100000 && !this.getAchievements().contains(Achievement.LOSE_HUNDRED_THOUSAND_SOCIOLES))
		{
			this.achievements.add(Achievement.LOSE_HUNDRED_THOUSAND_SOCIOLES);
			Achievement.LOSE_HUNDRED_THOUSAND_SOCIOLES.addAchievement(this);
		}
		if(profit < -1000000 && !this.getAchievements().contains(Achievement.LOSE_MILLION_SOCIOLES))
		{
			this.achievements.add(Achievement.LOSE_MILLION_SOCIOLES);
			Achievement.LOSE_MILLION_SOCIOLES.addAchievement(this);
		}
		if(profit < -1000000000 && !this.getAchievements().contains(Achievement.LOSE_BILLION_SOCIOLES))
		{
			this.achievements.add(Achievement.LOSE_BILLION_SOCIOLES);
			Achievement.LOSE_BILLION_SOCIOLES.addAchievement(this);
		}
		
		this.getActiveTrades().get(index).setTrade(null);
	}
	public void close(int index) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		if(this.getActiveTrades().size() > index)
		{
			if(this.getAlert(index) != null)
			{
				this.closeAlert(index);
			}
			if(this.getTrade(index) != null)
			{
				this.closeTrade(index);
			}

			this.activeTrades.remove(index);
		}
	}
	public EmbedBuilder displayAlert(int index) throws NumberFormatException, UnsupportedEncodingException, IOException
	{
		return this.activeTrades.get(index).getAlert().displayAlert();
	}
	public void levelUp()
	{
		this.level++;
	}
	public void saveUser()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		try
		{
			String json = gson.toJson(this);

			File file = new File("traders/" + this.getId() + ".json");
			
			if(!file.exists())
			{
				Main.botTrader.setBalance(Main.botTrader.getBalance() - 10);
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(json);
			bw.close();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void testAchievement()
	{
		double ratio;
		
		for(int key : nbTradesAchievements.keySet())
		{
			if(this.getNbTrades() == key && !this.achievements.contains(nbTradesAchievements.get(key)))
			{
				this.achievements.add(nbTradesAchievements.get(key));
				nbTradesAchievements.get(key).addAchievement(this);
			}
		}
		if(this.getNbTrades() == 5 && !this.achievements.contains(Achievement.FIVE_TRADE))
		{
			this.achievements.add(Achievement.FIVE_TRADE);
			Achievement.FIVE_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 10 && !this.achievements.contains(Achievement.TEN_TRADE))
		{
			this.achievements.add(Achievement.TEN_TRADE);
			Achievement.TEN_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 20 && !this.achievements.contains(Achievement.TWENTY_TRADE))
		{
			this.achievements.add(Achievement.TWENTY_TRADE);
			Achievement.TWENTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 30 && !this.achievements.contains(Achievement.THIRTY_TRADE))
		{
			this.achievements.add(Achievement.THIRTY_TRADE);
			Achievement.THIRTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 40 && !this.achievements.contains(Achievement.FOURTY_TRADE))
		{
			this.achievements.add(Achievement.FOURTY_TRADE);
			Achievement.FOURTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 50 && !this.achievements.contains(Achievement.FIFTY_TRADE))
		{
			this.achievements.add(Achievement.FIFTY_TRADE);
			Achievement.FIFTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 60 && !this.achievements.contains(Achievement.SIXTY_TRADE))
		{
			this.achievements.add(Achievement.SIXTY_TRADE);
			Achievement.SIXTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 70 && !this.achievements.contains(Achievement.SEVENTY_TRADE))
		{
			this.achievements.add(Achievement.SEVENTY_TRADE);
			Achievement.SEVENTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 80 && !this.achievements.contains(Achievement.EIGHTY_TRADE))
		{
			this.achievements.add(Achievement.EIGHTY_TRADE);
			Achievement.EIGHTY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 90 && !this.achievements.contains(Achievement.NINETY_TRADE))
		{
			this.achievements.add(Achievement.NINETY_TRADE);
			Achievement.NINETY_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 100 && !this.achievements.contains(Achievement.HUNDRED_TRADE))
		{
			this.achievements.add(Achievement.HUNDRED_TRADE);
			Achievement.HUNDRED_TRADE.addAchievement(this);
		}
		if(this.getNbTrades() == 1000 && !this.achievements.contains(Achievement.THOUSAND_TRADE))
		{
			this.achievements.add(Achievement.THOUSAND_TRADE);
			Achievement.THOUSAND_TRADE.addAchievement(this);
		}
		if(this.getFailedTrades() == 1 && !this.achievements.contains(Achievement.FIRST_FAILED_TRADE))
		{
			this.achievements.add(Achievement.FIRST_FAILED_TRADE);
			Achievement.FIRST_FAILED_TRADE.addAchievement(this);
		}
		if(this.getFailedTrades() == 10 && !this.achievements.contains(Achievement.TEN_FAILED_TRADE))
		{
			this.achievements.add(Achievement.TEN_FAILED_TRADE);
			Achievement.TEN_FAILED_TRADE.addAchievement(this);
		}
		if(this.getFailedTrades() == 100 && !this.achievements.contains(Achievement.HUNDRED_FAILED_TRADE))
		{
			this.achievements.add(Achievement.HUNDRED_FAILED_TRADE);
			Achievement.HUNDRED_FAILED_TRADE.addAchievement(this);
		}
		if(this.getSucceededTrades() == 1 && !this.achievements.contains(Achievement.FIRST_SUCCEEDED_TRADE))
		{
			this.achievements.add(Achievement.FIRST_SUCCEEDED_TRADE);
			Achievement.FIRST_SUCCEEDED_TRADE.addAchievement(this);
		}
		if(this.getSucceededTrades() == 10 && !this.achievements.contains(Achievement.TEN_SUCCEEDED_TRADE))
		{
			this.achievements.add(Achievement.TEN_SUCCEEDED_TRADE);
			Achievement.TEN_SUCCEEDED_TRADE.addAchievement(this);
		}
		if(this.getSucceededTrades() == 100 && !this.achievements.contains(Achievement.HUNDRED_SUCCEEDED_TRADE))
		{
			this.achievements.add(Achievement.HUNDRED_SUCCEEDED_TRADE);
			Achievement.HUNDRED_SUCCEEDED_TRADE.addAchievement(this);
		}
		if(this.getBalance() >= 100 && !this.achievements.contains(Achievement.REACH_HUNDRED_SOCIOLE))
		{
			this.achievements.add(Achievement.REACH_HUNDRED_SOCIOLE);
			Achievement.REACH_HUNDRED_SOCIOLE.addAchievement(this);
		}
		if(this.getBalance() >= 1000 && !this.achievements.contains(Achievement.REACH_THOUSAND_SOCIOLE))
		{
			this.achievements.add(Achievement.REACH_THOUSAND_SOCIOLE);
			Achievement.REACH_THOUSAND_SOCIOLE.addAchievement(this);
		}
		if(this.getBalance() >= 100000 && !this.achievements.contains(Achievement.REACH_HUNDRED_THOUSAND_SOCIOLE))
		{
			this.achievements.add(Achievement.REACH_HUNDRED_THOUSAND_SOCIOLE);
			Achievement.REACH_HUNDRED_THOUSAND_SOCIOLE.addAchievement(this);
		}
		if(this.getBalance() >= 1000000 && !this.achievements.contains(Achievement.REACH_MILLION_SOCIOLE))
		{
			this.achievements.add(Achievement.REACH_MILLION_SOCIOLE);
			Achievement.REACH_MILLION_SOCIOLE.addAchievement(this);
		}
		if(this.getBalance() >= 1000000000 && !this.achievements.contains(Achievement.REACH_BILLION_SOCIOLE))
		{
			this.achievements.add(Achievement.REACH_BILLION_SOCIOLE);
			Achievement.REACH_BILLION_SOCIOLE.addAchievement(this);
		}
		if(this.getLevel() >= 5 && !this.achievements.contains(Achievement.REACH_LEVEL_FIVE))
		{
			this.achievements.add(Achievement.REACH_LEVEL_FIVE);
			Achievement.REACH_LEVEL_FIVE.addAchievement(this);
		}		
		if(this.getLevel() >= 10 && !this.achievements.contains(Achievement.REACH_LEVEL_TEN))
		{
			this.achievements.add(Achievement.REACH_LEVEL_TEN);
			Achievement.REACH_LEVEL_TEN.addAchievement(this);
		}
		if(this.getLevel() >= 20 && !this.achievements.contains(Achievement.REACH_LEVEL_TWENTY))
		{
			this.achievements.add(Achievement.REACH_LEVEL_TWENTY);
			Achievement.REACH_LEVEL_TWENTY.addAchievement(this);
		}
		if(this.getLevel() >= 30 && !this.achievements.contains(Achievement.REACH_LEVEL_THIRTY))
		{
			this.achievements.add(Achievement.REACH_LEVEL_THIRTY);
			Achievement.REACH_LEVEL_THIRTY.addAchievement(this);
		}
		if(this.getLevel() >= 40 && !this.achievements.contains(Achievement.REACH_LEVEL_FOURTY))
		{
			this.achievements.add(Achievement.REACH_LEVEL_FOURTY);
			Achievement.REACH_LEVEL_FOURTY.addAchievement(this);
		}
		if(this.getLevel() >= 50 && !this.achievements.contains(Achievement.REACH_LEVEL_FIFTY))
		{
			this.achievements.add(Achievement.REACH_LEVEL_FIFTY);
			Achievement.REACH_LEVEL_FIFTY.addAchievement(this);
		}
		if(this.getActiveTrades().size() >= 10 && !this.achievements.contains(Achievement.HAVE_TEN_ACTIVE_TRADES))
		{
			this.achievements.add(Achievement.HAVE_TEN_ACTIVE_TRADES);
			Achievement.HAVE_TEN_ACTIVE_TRADES.addAchievement(this);
		}
		if(this.getNbTrades() > 0)
		{
			ratio = (((double)this.getSucceededTrades()/(double)this.getNbTrades())*100);
		}
		else
		{
			ratio = 0.0;
		}
		if(ratio >= 10 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_TEN))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_TEN);
			Achievement.RATIO_GREATER_THAN_ZERO_TEN.addAchievement(this);
		}
		if(ratio >= 20 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_TWENTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_TWENTY);
			Achievement.RATIO_GREATER_THAN_ZERO_TWENTY.addAchievement(this);
		}
		if(ratio >= 30 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_THIRTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_THIRTY);
			Achievement.RATIO_GREATER_THAN_ZERO_THIRTY.addAchievement(this);
		}
		if(ratio >= 40 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_FOURTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_FOURTY);
			Achievement.RATIO_GREATER_THAN_ZERO_FOURTY.addAchievement(this);
		}
		if(ratio >= 50 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_FIFTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_FIFTY);
			Achievement.RATIO_GREATER_THAN_ZERO_FIFTY.addAchievement(this);
		}
		if(ratio >= 60 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_SIXTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_SIXTY);
			Achievement.RATIO_GREATER_THAN_ZERO_SIXTY.addAchievement(this);
		}
		if(ratio >= 70 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY);
			Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY.addAchievement(this);
		}
		if(ratio >= 75 && this.getNbTrades() >= 10 && !this.achievements.contains(Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY_FIVE))
		{
			this.achievements.add(Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY_FIVE);
			Achievement.RATIO_GREATER_THAN_ZERO_SEVENTY_FIVE.addAchievement(this);
		}
		this.saveUser();
	}
	
	public static Trader getTrader(String id)
	{
		String json = "";
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		try(BufferedReader reader = new BufferedReader(new FileReader("traders/" + id + ".json")))
		{
			String line = "";
			while ((line = reader.readLine()) != null)
			{
				json  += line;
			}
		}
		catch(Exception e)
		{
			Trader user = new Trader(id);
			user.saveUser();
			try
			{
				Main.bot.getGuildById("312631087480438794").getController().addSingleRoleToMember(Main.bot.getGuildById("312631087480438794").getMemberById(id), Main.bot.getGuildById("312631087480438794").getRoleById("690699942502727771")).complete();
			}
			catch(Exception exception)
			{
				
			}
			return user;
		}
		Trader user = gson.fromJson(json, Trader.class);
		
		return user;
	}
}